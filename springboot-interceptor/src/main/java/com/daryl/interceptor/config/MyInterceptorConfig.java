package com.daryl.interceptor.config;

import com.daryl.interceptor.interceptor.MyInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

/**
 * 自定义拦截器的配置
 * 继承WebMvcConfigurationSupport会导致Spring Boot对mvc的自动配置失效，但可以用在前后端分离项目中
 * 如果需要让Spring Boot的自动配置生效，需要重写addResourceHandlers方法，将自动配置的路径放开
 *
 * WebMvcConfigurationSupport 会导致默认的静态资源被拦截，需要我们手动将静态资源放开。
 *
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/4/20 22:17
 */
//@Configuration
public class MyInterceptorConfig extends WebMvcConfigurationSupport {

    @Override
    protected void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new MyInterceptor()).addPathPatterns("/**");
        super.addInterceptors(registry);
    }

    /**
     * 用来指定静态资源不被拦截，否则继承WebMvcConfigurationSupport这种方式会导致静态资源无法直接访问
     *
     * @param registry
     */
    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");
        super.addResourceHandlers(registry);
    }
}
