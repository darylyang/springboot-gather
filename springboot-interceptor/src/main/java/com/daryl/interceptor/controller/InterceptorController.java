package com.daryl.interceptor.controller;

import com.daryl.interceptor.annotation.UnInterception;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/4/20 22:26
 */
@Controller
@RequestMapping("/interceptor")
public class InterceptorController {

    @RequestMapping("/test")
    public String test() {
        return "hello";
    }

    @UnInterception
    @RequestMapping("/test2")
    @ResponseBody
    public String test2() {
        return "not intercepted";
    }
}
