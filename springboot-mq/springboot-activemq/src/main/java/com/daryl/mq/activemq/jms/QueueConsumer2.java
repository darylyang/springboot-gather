package com.daryl.mq.activemq.jms;

import com.daryl.mq.activemq.config.ActiveMqConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Service;

/**
 * 消息消费者
 */
@Slf4j
@Service
public class QueueConsumer2 {

    /**
     * SendTo 该注解的意思是将return回的值，再发送的"out.queue"队列中
     * 接收点对点消息
     *
     * @param msg
     */
    @JmsListener(destination = ActiveMqConfig.QUEUE_NAME)
    @SendTo("out.queue")
    public String receiveQueueMsg(String msg) {
        log.info("2收到的消息为：" + msg);
        return "2---收到的消息为：" + msg;
    }
}
