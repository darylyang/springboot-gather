package com.daryl.mq.activemq.jms;

import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;

import javax.annotation.Resource;
import javax.jms.Destination;
import java.util.Map;

/**
 * 消息发送者
 */
@Service
public class MsgProducer {

    @Resource
    private JmsMessagingTemplate jmsTemplate;

    public void sendMessage(Destination destination, String msg) {
        jmsTemplate.convertAndSend(destination, msg);
    }

    public void sendDelayMessage(Destination destination, String msg, Map<String, Object> headers) {
        jmsTemplate.convertAndSend(destination, msg, headers);
    }

    /**
     * 异步发送消息
     *
     * @param destination
     * @param msg
     * @return
     */
    @Async
    public ListenableFuture<Void> asyncSend(Destination destination, String msg) {
        try {
            // 发送消息
            this.sendMessage(destination, msg);
            // 返回成功的 Future
            return AsyncResult.forValue(null);
        } catch (Throwable ex) {
            // 返回异常的 Future
            return AsyncResult.forExecutionException(ex);
        }
    }

}
