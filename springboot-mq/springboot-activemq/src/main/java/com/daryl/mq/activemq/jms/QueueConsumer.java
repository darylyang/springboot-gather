package com.daryl.mq.activemq.jms;

import com.daryl.mq.activemq.config.ActiveMqConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

/**
 * 消息消费者
 */
@Slf4j
@Service
public class QueueConsumer {

    /**
     * concurrency 并行消费
     * 接收点对点消息
     *
     * @param msg
     */
    //    @JmsListener(destination = ActiveMqConfig.QUEUE_NAME)
    @JmsListener(destination = ActiveMqConfig.QUEUE_NAME, concurrency = "3")
    public void receiveQueueMsg(String msg) {
        log.info("1收到的消息为：" + msg);
    }
}
