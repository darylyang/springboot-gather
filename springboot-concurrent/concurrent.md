
[TOC]

## 队列
阻塞队列
ArrayBlockingQueue		：有界
LinkedBlockingQueue		：无界
SynchronousQueue			：无内存直接用
<p>
非阻塞队列
ConcurrentLinkedQueue	：高性能


## BlockingQueue

BlockingQueue的核心方法：

放入数据：
- offer(anObject):表示如果可能的话,将anObject加到BlockingQueue里,即如果BlockingQueue可以容纳,则返回true,否则返回false.（本方法不阻塞当前执行方法的线程）
- offer(E o, long timeout, TimeUnit unit),可以设定等待的时间，如果在指定的时间内，还不能往队列中加入BlockingQueue，则返回失败。
- put(anObject):把anObject加到BlockingQueue里,如果BlockQueue没有空间,则调用此方法的线程被阻断直到BlockingQueue里面有空间再继续.

获取数据：
- poll(time):取走BlockingQueue里排在首位的对象,若不能立即取出,则可以等time参数规定的时间,取不到时返回null;
- poll(long timeout, TimeUnit unit)：从BlockingQueue取出一个队首的对象，如果在指定时间内，队列一旦有数据可取，则立即返回队列中的数据。否则知道时间超时还没有数据可取，返回失败。
- take():取走BlockingQueue里排在首位的对象,若BlockingQueue为空,阻断进入等待状态直到BlockingQueue有新的数据被加入; 
- drainTo():一次性从BlockingQueue获取所有可用的数据对象（还可以指定获取数据的个数），通过该方法，可以提升获取数据效率；不需要多次分批加锁或释放锁。



### ArrayBlockingQueue
ArrayBlockingQueue : 基于数组的阻塞队列实现，在内部维护了一个定长数组，以便缓存队列中的数据对象。
内部没有实现读写分离，生产和消费不能完全并行，
长度是需要定义的，
可以指定先进先出或者先进后出，
是一个有界队列。
<p>
offer 插入数据方法---成功返回true 否则返回false
boolean offer(E e)
boolean offer(E e, long timeout, TimeUnit unit)
array.offer("offer 3秒后插入数据方法", 3, TimeUnit.SECONDS);
<p>
array.put("put 插入数据方法---但超出队列长度则阻塞等待，没有返回值"); // 超出长度就 阻塞了。。。
<p>
//  java.lang.IllegalStateException: Queue full
array.add("add 插入数据方法---但超出队列长度则提示 java.lang.IllegalStateException");


### LinkedBlockingQueue
LinkedBlockingQueue：基于列表的阻塞队列，在内部维护了一个数据缓冲队列（该队列由一个链表构成）。
其内部实现采用读写分离锁，能高效的处理并发数据，生产者和消费者操作的完全并行运行
可以不指定长度，
是一个无界队列。


### SynchronousQueue
SynchronousQueue：没有缓冲的队列，生存者生产的数据直接会被消费者获取并消费。


### ConcurrentLinkedQueue
ConcurrentLinkedQueue：是一个适合高并发场景下的队列，通过无锁的方式，实现了高并发状态下的高性能，性能好于BlockingQueue。
它是一个基于链接节点的无界限线程安全队列。该队列的元素遵循先进先出的原则。头是最先加入的，尾是最后加入的，不允许null元素。
无阻塞队列，没有 put 和 take 方法



