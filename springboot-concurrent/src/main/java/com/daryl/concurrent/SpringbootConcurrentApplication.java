package com.daryl.concurrent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootConcurrentApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootConcurrentApplication.class, args);
    }

}
