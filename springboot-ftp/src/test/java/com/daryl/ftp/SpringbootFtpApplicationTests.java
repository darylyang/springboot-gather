package com.daryl.ftp;

import com.daryl.ftp.service.FtpProcessor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@SpringBootTest
class SpringbootFtpApplicationTests {

    private static final String YYYY_MM_DD          = "yyyyMMdd";
    private static final String YYYY_MM_DD_HH_MM_SS = "yyyyMMdd_HHmmss";

    public static String getLocalDate() {
        return LocalDate.now().format(DateTimeFormatter.ofPattern(YYYY_MM_DD));
    }

    public static String getLocalDateTime() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern(YYYY_MM_DD_HH_MM_SS));
    }

    @Test
    void contextLoads() {
    }

    @Autowired
    private FtpProcessor ftpProcessor;

    @Test
    public void test() throws IOException {
        File file = new File("G:\\ftp\\2019.txt");
        System.out.println(file.getAbsolutePath());
        System.out.println(file.getPath());
        System.out.println(file.getCanonicalPath());
    }

    @Test
    public void testUpload() {
        boolean b = ftpProcessor.uploadFile("/111", getLocalDate() + ".txt", "G:\\ftp\\2019.txt");
        System.out.println(b);
    }

    @Test
    public void testDownload() {
        boolean b = ftpProcessor.downloadFile("\\111", getLocalDate() + ".txt", "G:\\ftp\\");
        System.out.println(b);
    }

    @Test
    public void testDelete() {
        boolean deleteFile = ftpProcessor.deleteFile("\\111", getLocalDate() + ".txt");
        System.out.println(deleteFile);
    }
}
