package com.daryl.ftp.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "ftp")
public class FtpProperties {

    private String ip;
    private String port;
    private String username;
    private String password;
    private Integer initialSize = 0;
    private String encoding = "UTF-8";
    private Integer bufferSize = 4096;
    private Integer retryCount = 3;

}
