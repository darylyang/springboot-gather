package com.daryl.ftp;

import com.daryl.ftp.service.FtpProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/4/11 12:45
 */
@RestController("ftp")
public class FtpController {

    @Autowired
    private FtpProcessor ftpProcessor;

    @PostMapping("/uploadFile")
    public boolean uploadFile(String path, String fileName, String originFileName) {
        return ftpProcessor.uploadFile(path, fileName, originFileName);
    }

    public boolean uploadFile(String path, String fileName, InputStream inputStream) {
        return ftpProcessor.uploadFile(path, fileName, inputStream);
    }

    @PostMapping("/downloadFile")
    public boolean downloadFile(String path, String fileName, String localPath) {
        return ftpProcessor.downloadFile(path, fileName, localPath);
    }

    @PostMapping("/deleteFile")
    public boolean deleteFile(String path, String fileName) {
        return ftpProcessor.deleteFile(path, fileName);
    }

    public boolean createDirectory(String remote) throws IOException {
        return ftpProcessor.createDirectory(remote);
    }

    public boolean existFile(String path) throws IOException {
        return ftpProcessor.existFile(path);
    }

    public boolean makeDirectory(String directory) {
        return ftpProcessor.makeDirectory(directory);
    }

    public List<String> retrieveFileNames(String remotePath) throws IOException {
        return ftpProcessor.retrieveFileNames(remotePath);
    }
}

