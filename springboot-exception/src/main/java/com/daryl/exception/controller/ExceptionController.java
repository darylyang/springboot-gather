package com.daryl.exception.controller;

import com.daryl.exception.config.BusinessMsgEnum;
import com.daryl.exception.entity.JsonResult;
import com.daryl.exception.exception.BusinessErrorException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/4/18 22:05
 */
@Slf4j
@RestController
@RequestMapping("/exception")
public class ExceptionController {

    @GetMapping("/null/point/exception")
    public JsonResult testNullPointException() {
        String str = null;
        str.length();
        return new JsonResult();
    }

    @PostMapping("/test")
    public JsonResult test(@RequestParam("name") String name, @RequestParam("pass") String pass) {
        log.info("name：{}", name);
        log.info("pass：{}", pass);
        return new JsonResult();
    }

    @GetMapping("/business")
    public JsonResult testException() {
        try {
            int i = 1 / 0;
        } catch (Exception e) {
            throw new BusinessErrorException(BusinessMsgEnum.UNEXPECTED_EXCEPTION);
        }
        return new JsonResult();
    }
}
