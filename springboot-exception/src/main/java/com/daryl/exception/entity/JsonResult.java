package com.daryl.exception.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 统一返回的异常封装实体
 *
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/4/18 21:25
 */
@Data
@AllArgsConstructor
public class JsonResult {

    /**
     * 异常码
     */
    protected String code;

    /**
     * 异常信息
     */
    protected String msg;

    public JsonResult() {
        this.code = "200";
        this.msg = "操作成功";
    }


    //    public JsonResult(BusinessErrorException ex) {
    //        this.code = ex.getCode();
    //        this.msg = ex.getMessage();
    //    }
    //
    //    public JsonResult(BusinessMsgEnum msg) {
    //        this.code = msg.code();
    //        this.msg = msg.msg();
    //    }


}
