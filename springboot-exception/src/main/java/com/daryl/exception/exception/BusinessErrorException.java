package com.daryl.exception.exception;

import com.daryl.exception.config.BusinessMsgEnum;
import lombok.Data;

/**
 * 自定义业务异常
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/4/18 21:37
 */
@Data
public class BusinessErrorException extends RuntimeException{

    /**
     * 异常码
     */
    private String code;

    /**
     * 异常提示信息
     */
    private String message;

    public BusinessErrorException(BusinessMsgEnum businessMsgEnum) {
        this.code = businessMsgEnum.code();
        this.message = businessMsgEnum.msg();
    }
}
