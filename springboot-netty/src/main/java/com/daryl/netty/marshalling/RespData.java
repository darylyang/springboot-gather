package com.daryl.netty.marshalling;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class RespData implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String name;
    private String responseMsg;

}