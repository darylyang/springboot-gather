package com.daryl.netty.marshalling;

import com.daryl.netty.utils.Util;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileOutputStream;

@Slf4j
public class ServerHandler extends ChannelInboundHandlerAdapter {

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		log.info("Netty Server active ......");
	}

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		try {
			// 获取客户端传来的数据
			ReqData requestData = (ReqData) msg;
			log.info("Netty Server : " + requestData.toString());
			// 处理数据并返回给客户端
			RespData responseData = new RespData();
			responseData.setId(requestData.getId());
			responseData.setName(requestData.getName() + "-SUCCESS!");
			responseData.setResponseMsg(requestData.getRequestMsg() + "-SUCCESS!");
			// 如果有附件则保存附件
			if (null != requestData.getAttachment()) {
				byte[] attachment = Util.ungzip(requestData.getAttachment());
//				String path = System.getProperty("user.dir") + File.separatorChar + "receive" +
//						File.separatorChar + System.currentTimeMillis() + ".jpg";
				String fileName = System.currentTimeMillis() + ".jpg";
				String path = "G:\\DEVELOP\\00gitee\\springboot-gather\\springboot-netty\\src\\main\\resources\\img" + File.separator + fileName;
				FileOutputStream outputStream = new FileOutputStream(path);
				outputStream.write(attachment);
				outputStream.close();
				log.info("file upload success , file path is : " + path);
				responseData.setResponseMsg("file upload success , file path is : " + path);
			}
			ctx.writeAndFlush(responseData);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ReferenceCountUtil.release(msg);
		}
	}

	@Override
	public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		cause.printStackTrace();
		ctx.close();
	}

}
