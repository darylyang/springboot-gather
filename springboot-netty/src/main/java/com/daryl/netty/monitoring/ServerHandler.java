package com.daryl.netty.monitoring;

import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;

@Slf4j
public class ServerHandler extends ChannelInboundHandlerAdapter {

    // 令牌验证的map，key为ip地址，value为密钥
    private static HashMap<String, String> authMap = new HashMap<String, String>();

    // 模拟数据库查询
    static {
        authMap.put("xxx.xxx.x.x", "xxx");
        authMap.put(CoreParam.CLIENT_HOST.getValue(), CoreParam.SALT_KEY.getValue());
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        log.info("Netty Server Monitoring.......");
    }

    /**
     * 模拟api请求前的验证
     *
     * @param ctx
     * @param msg
     * @return
     */
    private boolean auth(ChannelHandlerContext ctx, Object msg) {
        log.info("令牌验证...............");
        String[] ret = ((String) msg).split(",");
        // 客户端ip地址
        String clientIp = ret[0];
        // 数据库保存的客户端密钥
        String saltKey = ret[1];
        // 客户端传来的密钥
        String auth = authMap.get(clientIp);
        if (null != auth && auth.equals(saltKey)) {
            ctx.writeAndFlush(CoreParam.AUTH_SUCCESS.getValue());
            return true;
        } else {
            ctx.writeAndFlush(CoreParam.AUTH_ERROR.getValue()).addListener(ChannelFutureListener.CLOSE);
            return false;
        }
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        // 如果传来的消息是字符串，则先验证
        log.info("RequestInfo:{}", msg.toString());
        if (msg instanceof String) {
            auth(ctx, msg);
        } else if (msg instanceof RequestInfo) {
            RequestInfo info = (RequestInfo) msg;
            log.info("--------------------------------------------");
            log.info("当前主机ip为: " + info.getIp());
            HashMap<String, Object> cpu = info.getCpuPercMap();
            log.info("cpu 总使用率: " + cpu.get(CoreParam.COMBINED.getValue()));
            log.info("cpu 用户使用率: " + cpu.get(CoreParam.USER.getValue()));
            log.info("cpu 系统使用率: " + cpu.get(CoreParam.SYS.getValue()));
            log.info("cpu 等待率: " + cpu.get(CoreParam.WAIT.getValue()));
            log.info("cpu 空闲率: " + cpu.get(CoreParam.IDLE.getValue()));

            HashMap<String, Object> memory = info.getMemoryMap();
            log.info("内存总量: " + memory.get(CoreParam.TOTAL.getValue()));
            log.info("当前内存使用量: " + memory.get(CoreParam.USED.getValue()));
            log.info("当前内存剩余量: " + memory.get(CoreParam.FREE.getValue()));
            log.info("--------------------------------------------");

            ctx.writeAndFlush("info received!");
        } else {
            ctx.writeAndFlush("connect failure!").addListener(ChannelFutureListener.CLOSE);
        }
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }

}


/*
15:34:40.818 [nioEventLoopGroup-3-1] INFO com.daryl.netty.monitoring.ServerHandler - --------------------------------------------
15:34:51.320 [nioEventLoopGroup-3-1] INFO com.daryl.netty.monitoring.ServerHandler - RequestInfo:RequestInfo(ip=192.168.219.100, cpuPercMap={wait=0.0, idle=0.7, combined=0.2852216748768473, sys=0.1310344827586207, user=0.1541871921182266}, memoryMap={total=33450852, used=17168608, free=16282244})
15:34:51.320 [nioEventLoopGroup-3-1] INFO com.daryl.netty.monitoring.ServerHandler - --------------------------------------------
15:34:51.320 [nioEventLoopGroup-3-1] INFO com.daryl.netty.monitoring.ServerHandler - 当前主机ip为: 192.168.219.100
15:34:51.321 [nioEventLoopGroup-3-1] INFO com.daryl.netty.monitoring.ServerHandler - cpu 总使用率: 0.2852216748768473
15:34:51.321 [nioEventLoopGroup-3-1] INFO com.daryl.netty.monitoring.ServerHandler - cpu 用户使用率: 0.1541871921182266
15:34:51.321 [nioEventLoopGroup-3-1] INFO com.daryl.netty.monitoring.ServerHandler - cpu 系统使用率: 0.1310344827586207
15:34:51.321 [nioEventLoopGroup-3-1] INFO com.daryl.netty.monitoring.ServerHandler - cpu 等待率: 0.0
15:34:51.321 [nioEventLoopGroup-3-1] INFO com.daryl.netty.monitoring.ServerHandler - cpu 空闲率: 0.7
15:34:51.321 [nioEventLoopGroup-3-1] INFO com.daryl.netty.monitoring.ServerHandler - 内存总量: 33450852
15:34:51.321 [nioEventLoopGroup-3-1] INFO com.daryl.netty.monitoring.ServerHandler - 当前内存使用量: 17168608
15:34:51.321 [nioEventLoopGroup-3-1] INFO com.daryl.netty.monitoring.ServerHandler - 当前内存剩余量: 16282244
15:34:51.321 [nioEventLoopGroup-3-1] INFO com.daryl.netty.monitoring.ServerHandler - --------------------------------------------
15:35:01.823 [nioEventLoopGroup-3-1] INFO com.daryl.netty.monitoring.ServerHandler - RequestInfo:RequestInfo(ip=192.168.219.100, cpuPercMap={wait=0.0, idle=0.535483870967742, combined=0.456575682382134, sys=0.1935483870967742, user=0.2630272952853598}, memoryMap={total=33450852, used=17175724, free=16275128})
15:35:01.823 [nioEventLoopGroup-3-1] INFO com.daryl.netty.monitoring.ServerHandler - --------------------------------------------
15:35:01.823 [nioEventLoopGroup-3-1] INFO com.daryl.netty.monitoring.ServerHandler - 当前主机ip为: 192.168.219.100
15:35:01.823 [nioEventLoopGroup-3-1] INFO com.daryl.netty.monitoring.ServerHandler - cpu 总使用率: 0.456575682382134
15:35:01.823 [nioEventLoopGroup-3-1] INFO com.daryl.netty.monitoring.ServerHandler - cpu 用户使用率: 0.2630272952853598
15:35:01.823 [nioEventLoopGroup-3-1] INFO com.daryl.netty.monitoring.ServerHandler - cpu 系统使用率: 0.1935483870967742
15:35:01.823 [nioEventLoopGroup-3-1] INFO com.daryl.netty.monitoring.ServerHandler - cpu 等待率: 0.0
15:35:01.823 [nioEventLoopGroup-3-1] INFO com.daryl.netty.monitoring.ServerHandler - cpu 空闲率: 0.535483870967742
15:35:01.823 [nioEventLoopGroup-3-1] INFO com.daryl.netty.monitoring.ServerHandler - 内存总量: 33450852
15:35:01.823 [nioEventLoopGroup-3-1] INFO com.daryl.netty.monitoring.ServerHandler - 当前内存使用量: 17175724
15:35:01.823 [nioEventLoopGroup-3-1] INFO com.daryl.netty.monitoring.ServerHandler - 当前内存剩余量: 16275128
15:35:01.823 [nioEventLoopGroup-3-1] INFO com.daryl.netty.monitoring.ServerHandler - --------------------------------------------



* */