package com.daryl.netty.monitoring;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.HashMap;

@Data
@ToString
public class RequestInfo implements Serializable {
  
	private static final long serialVersionUID = 1L;
	private String ip ;  
    private HashMap<String, Object> cpuPercMap ;  
    private HashMap<String, Object> memoryMap;

}
