package com.daryl.netty.monitoring;

import com.daryl.netty.marshalling.MyMarshallerFactory;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

public class Client {

    public static void main(String[] args) {
        NioEventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            Bootstrap bootstrap = new Bootstrap();
            bootstrap.group(workerGroup).channel(NioSocketChannel.class).handler(new ChannelInitializer<SocketChannel>() {
                @Override
                protected void initChannel(SocketChannel socketChannel) throws Exception {
                    socketChannel.pipeline().addLast(MyMarshallerFactory.buildMarshallingDecoder());
                    socketChannel.pipeline().addLast(MyMarshallerFactory.builMarshallingEncoder());
                    socketChannel.pipeline().addLast(new ClientHandler());
                }
            }).option(ChannelOption.SO_KEEPALIVE, true);
            // 建立连接
            ChannelFuture future = bootstrap.connect(CoreParam.SERVER_HOST.getValue(), Integer.parseInt(CoreParam.PORT.getValue())).sync();
            future.channel().closeFuture().sync();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            workerGroup.shutdownGracefully();
        }
    }

}  