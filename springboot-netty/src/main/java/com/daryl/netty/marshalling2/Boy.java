package com.daryl.netty.marshalling2;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/4/12 17:50
 */
@Data
@ToString
public class Boy implements Serializable {

    private static final long serialVersionUID = 3333140133888151024L;
    /**
     * 姓名
     */
    private String name;

    /**
     *  身高
     */
    private int height;

    /**
     *  颜值
     */
    private int faceScore;

    /**
     *  是否富有
     */
    private boolean isRich;
}
