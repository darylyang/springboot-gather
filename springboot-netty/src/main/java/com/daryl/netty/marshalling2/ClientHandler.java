package com.daryl.netty.marshalling2;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.extern.slf4j.Slf4j;

import java.util.Random;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/4/12 18:03
 */
@Slf4j
public class ClientHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        log.info("五个蓝孩子组队去表白：");
        Random r = new Random();
        for (int i = 1; i <= 5; i++) {
            Boy boy = new Boy();
            boy.setName("同学 " + i);
            boy.setFaceScore(r.nextInt(20) + 80);
            boy.setHeight(r.nextInt(25) + 160);
            boy.setRich(i % 2 == 0);
            ctx.write(boy);
            log.info(boy.toString());
        }
        ctx.flush();
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        GirlResponse gr = (GirlResponse) msg;
        log.info("收到女生回复 ："+gr.getMsg());
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }
}
