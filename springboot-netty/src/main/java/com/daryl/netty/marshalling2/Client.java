package com.daryl.netty.marshalling2;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.timeout.ReadTimeoutHandler;

public class Client {

    private static final Integer PORT = 8866;
    private static final String HOST = "127.0.0.1";
    private EventLoopGroup group = null;
    private Bootstrap bootstrap = null;
    private ChannelFuture future = null;

    /**
     * 定义一个私有的静态内部类 负责初始化客户端
     */
    private static class SingletonHolder {
        static final Client instance = new Client();
    }

    public static Client getInstance() {
        return SingletonHolder.instance;
    }

    public Client() {
        group = new NioEventLoopGroup();
        bootstrap = new Bootstrap();
        try {
            bootstrap.group(group).channel(NioSocketChannel.class).handler(new ChannelInitializer<SocketChannel>() {
                @Override
                protected void initChannel(SocketChannel socketChannel) throws Exception {
                    // 配置编码器
                    socketChannel.pipeline().addLast(MyMarshallerFactory.buildMarshallingDecoder());
                    // 配置解码器
                    socketChannel.pipeline().addLast(MyMarshallerFactory.builMarshallingEncoder());
                    socketChannel.pipeline().addLast(new ReadTimeoutHandler(5));
                    socketChannel.pipeline().addLast(new ClientHandler());
                }
            }).option(ChannelOption.SO_BACKLOG, 1024);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void connect() {
        try {
            future = bootstrap.connect(HOST, PORT).sync();
            System.out.println("连接远程服务器......");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ChannelFuture getChannelFuture() {
        if (this.future == null || !this.future.channel().isActive()) {
            this.connect();
        }
        return this.future;
    }

    /**
     * 特殊长连接：
     * 1. 服务器和客户端的通道一直处于开启状态，
     * 2. 在服务器指定时间内，没有任何通信，则断开，
     * 3. 客户端再次向服务端发送请求则重新建立连接，
     * 4. 从而减小服务端资源占用压力。
     */
    public static void main(String[] args) {
        // 线程调用需要加final
        final Client client = Client.getInstance();

        ChannelFuture future = client.getChannelFuture();

        try {
            future.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}
