package com.daryl.netty.marshalling2;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.extern.slf4j.Slf4j;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/4/12 17:46
 */
@Slf4j
@ChannelHandler.Sharable
public class ServerHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        Boy boy = (Boy) msg;
        log.info("收到表白：" + boy);
        ctx.writeAndFlush(reply(boy));
    }

    private static GirlResponse reply(Boy boy) {
        GirlResponse gr = new GirlResponse();
        if (boy.getFaceScore() >= 90 || boy.getHeight() >= 180 || boy.isRich()) {
            gr.setMsg("to " + boy.getName() + " : 中意你哦！");
        } else {
            gr.setMsg("to " + boy.getName() + " : 骚年回家继续努力吧！");
        }
        return gr;
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }
}
