package com.daryl.netty.marshalling2;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/4/12 17:51
 */
@Data
@ToString
public class GirlResponse implements Serializable {

    private static final long serialVersionUID = -2619994978640439932L;

    private int code;

    private String msg;

}
