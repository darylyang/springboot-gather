package com.daryl.netty.netty01;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.AttributeKey;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/2/5 12:06
 */
public class ClientHandler extends ChannelInboundHandlerAdapter {
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
//        super.channelRead(ctx, msg);
        System.out.println("客户端开始读取数据============");

        System.out.println("接收到服务器的数据为：" + msg.toString());
        ctx.channel().attr(AttributeKey.valueOf("ChannelKey")).set(msg.toString() + "qwer");
        ctx.channel().close();
    }
}
