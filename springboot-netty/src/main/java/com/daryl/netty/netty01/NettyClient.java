package com.daryl.netty.netty01;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.util.AttributeKey;

import java.util.concurrent.TimeUnit;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/2/5 11:41
 */
public class NettyClient {

    private static EventLoopGroup group = null;
    private static Bootstrap bootstrap = null;

    static {
        group = new NioEventLoopGroup();
        bootstrap = new Bootstrap();
        bootstrap.channel(NioSocketChannel.class);
        bootstrap.group(group);
        bootstrap.option(ChannelOption.SO_KEEPALIVE, true)
                .handler(new ChannelInitializer<NioSocketChannel>() {
                    @Override
                    protected void initChannel(NioSocketChannel ch) throws Exception {
                        ch.pipeline().addLast(new DelimiterBasedFrameDecoder(Integer.MAX_VALUE, Delimiters.lineDelimiter()[0]));
                        ch.pipeline().addLast(new StringDecoder());
                        ch.pipeline().addLast(new ClientHandler());
                        ch.pipeline().addLast(new StringEncoder());
                    }
                });
    }

    public static void main(String[] args) {

        try {
            ChannelFuture future = bootstrap.connect("localhost", 8080).sync();

            String person = "张三\r\n";
            future.channel().writeAndFlush(person);
//            future.channel().writeAndFlush(person.getBytes(Charset.forName("UTF-8")));
//            future.channel().writeAndFlush(Delimiters.lineDelimiter()[0]);
//            future.channel().closeFuture().sync();

            TimeUnit.SECONDS.sleep(5);

            Object result = future.channel().attr(AttributeKey.valueOf("ChannelKey")).get();
            System.out.println(result);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            group.shutdownGracefully();
        }
    }
}
