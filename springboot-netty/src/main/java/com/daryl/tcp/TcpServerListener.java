package com.daryl.tcp;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.Socket;
import java.net.SocketException;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/3/30 22:48
 */
public class TcpServerListener extends Thread {

    private static Logger logger = LoggerFactory.getLogger(TcpServerListener.class);
    private static String CHARSET = "UTF-8";
    private Socket client;

    TcpServerListener(Socket client) {
        this.client = client;
        try {
            client.setSoTimeout(1000);
        } catch (SocketException e) {
            logger.warn("Error while setting soTimeout to " + 1000);
            e.printStackTrace();
        }
        this.setName("TcpServerListener-" + client.getRemoteSocketAddress());
    }

    @Override
    public void run() {
        BufferedReader reader = null;
        PrintWriter writer = null;
        try {
            // 双工 读写
            reader = new BufferedReader(new InputStreamReader(client.getInputStream(), CHARSET));
            String reqMsg = reader.readLine();
            logger.info("server recive client message:" + reqMsg);
            writer = new PrintWriter(new OutputStreamWriter(client.getOutputStream(), CHARSET));
            handleRecvMessage(reqMsg, writer);
        } catch (IOException e) {
            e.printStackTrace();
            logger.warn("a control connection broke", e);
        } finally {
            IOUtils.closeQuietly(writer);
            IOUtils.closeQuietly(reader);
            IOUtils.closeQuietly(client);
        }
    }

    private void handleRecvMessage(String msg, Writer writer) {
        String respMsg = "";
        // 响应
        respMsg = "i have received " + msg;
        logger.info("server response message:" + respMsg);
        // return execute result to collector
        try {
            writer.write(respMsg);
            writer.flush();
        } catch (IOException e) {
            logger.error(e + "write response to collector error!");
        }
    }
}
