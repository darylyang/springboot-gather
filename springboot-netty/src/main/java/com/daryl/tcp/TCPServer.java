package com.daryl.tcp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/3/30 22:48
 */
public class TCPServer extends Thread {

    /**
     * 线程池用于 处理每个客户端连接
     */
    private static final ExecutorService pool = Executors.newFixedThreadPool(10);
    private static Logger logger = LoggerFactory.getLogger(TCPServer.class);
    private ServerSocket server = null;
    private volatile boolean closing = false;

    public static void main(String[] args) {
        pool.submit(new TCPServer());
    }

    @Override
    public void run() {
        try {
            if (!isBound()) {
                tryToBind();
            }
        } catch (IOException e) {
            return;
        }

        while (!closing) {
            try {
                Socket client = server.accept();
                if (logger.isDebugEnabled()) {
                    logger.debug("server recieve message from " + client.getInetAddress());
                }
                // 提交监听器线程 来处理客户端的连接
                TcpServerListener listener = new TcpServerListener(client);
                // listener.setDaemon(true);
                pool.submit(listener);
            } catch (IOException e) {
                if (!closing)
                    logger.warn("control socket error: ", e);
                else {
                    logger.warn("shutting down listen thread due to shutdown() call");
                    break;
                }
            }
        }// 结束循环

    }

    public boolean isBound() {
        return server != null && server.isBound();
    }

    public void tryToBind() throws IOException {
        // ServerSocket 绑定服务端口
        server = new ServerSocket(8200);
        server.setReuseAddress(true);
        if (server.isBound()) {
            logger.info("server socket bound to " + server.getLocalPort());
        } else {
            logger.info("server socket isn't bound");
        }
    }

    /**
     * 关闭tcp ServerSocket
     */
    public void shutdown() {
        closing = true;
        try {
            // 防止NPE
            if (server != null) {
                server.close();
            }
            server = null;
        } catch (IOException e) {
            logger.error(e + "shutdown tcp server failed");
        }
    }

}
