package com.daryl.tcp;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.Socket;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/3/30 22:50
 */
public class TcpClient extends Thread {
    private static final Logger logger = LoggerFactory.getLogger(TcpClient.class);

    private static final ExecutorService pool = Executors.newFixedThreadPool(10);
    /**
     * 连接超时，默认5000ms
     */
    private static final int connectTimeout = 5000;

    private static final int DEFAULT_READE_TIMEOUT = 10000;

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            pool.submit(new TcpClient());
        }
    }

    @Override
    public void run() {
        String host = "localhost";
        String port = "8200";
        while (true) {
            String request = "我是" + Thread.currentThread().getName() + "客户端";
            String resp = new TcpClient().post(host, port, request, "UTF-8");
            logger.info("收到响应：" + resp);
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    public String post(String host, String port, String request, String charset) {
        String serviceResponse = null;
        try {
            // 发送请求，拿到响应
            serviceResponse = send(request, charset, host, Integer.valueOf(port)).get();
        } catch (Exception e) {
            return "发送失败：" + host;
        }
        return serviceResponse;
    }

    // 发送数据
    public Future<String> send(String requestMsg, String charset, String ip, int port) {
        Future<String> future = pool.submit(new SendCall(requestMsg, charset, ip, port));
        return future;
    }

    public String sendMessage(String msg, String charset, String host, int port) {
        BufferedReader reader = null;
        BufferedWriter writer = null;
        Socket socket = null;
        try {
            socket = new Socket(host, port);
            // 设置读取超时时间
            socket.setSoTimeout(DEFAULT_READE_TIMEOUT);
            // 写入请求
            writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), charset));
            logger.info("send data to {}:{}", new Object[]{host, port});
            logger.info("发送 REQUEST:{}", msg);
            writer.write(msg + "\n");
            writer.flush();
            // 读取响应
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), charset));
            String result = reader.readLine();
            return result;

        } catch (IOException e) {
            logger.error(e + "发送消息失败");
        } finally {
            // 静默关闭
            IOUtils.closeQuietly(reader);
            IOUtils.closeQuietly(writer);
            IOUtils.closeQuietly(socket);
        }
        return null;
    }

    /**
     * 发送任务
     */
    private class SendCall implements Callable<String> {
        private final String requestMsg;
        private final String charset;
        private final String ip;
        private final int port;

        /**
         *
         */
        public SendCall(String requestMsg, String charset, String ip, int port) {
            super();
            this.requestMsg = requestMsg;
            this.charset = charset;
            this.ip = ip;
            this.port = port;
        }

        /**
         * 客户端 发送任务
         *
         * @see Callable#call()
         */
        @Override
        public String call() throws Exception {
            String resp;
            try {
                resp = sendMessage(requestMsg, charset, ip, port);
            } catch (Exception e) {
                String failMsg = "发送失败，ip:" + this.ip + "port:" + this.port;
                return failMsg;
            }
            return resp;
        }
    }

}

