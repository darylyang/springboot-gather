
[TOC]

# netty

## JBoss Marshalling ????

Marshalling ?????????????????

JBoss Marshalling ??? Java ???????? JDK ??????????????????? Serializable ?????????????????????????????????????????

JBoss Marshalling ????? ?????? netty ??

maven??
```pom
<dependency>
    <groupId>io.netty</groupId>
    <artifactId>netty-all</artifactId>
    <version>4.1.48.Final</version>
</dependency>
<dependency>
    <groupId>org.jboss.marshalling</groupId>
    <artifactId>jboss-marshalling</artifactId>
    <version>2.0.9.Final</version>
</dependency>
<dependency>
    <groupId>org.jboss.marshalling</groupId>
    <artifactId>jboss-marshalling-river</artifactId>
    <version>2.0.9.Final</version>
</dependency>
<dependency>
    <groupId>org.jboss.marshalling</groupId>
    <artifactId>jboss-marshalling-serial</artifactId>
    <version>2.0.9.Final</version>
</dependency>
```

### MarshallingDecoder ??
MarshallingDecoder ???LengthFieldBaseFrameDecoder, ??netty ??????????????????????????????????????

```java
public class MarshallingDecoder extends LengthFieldBasedFrameDecoder {
    private final UnmarshallerProvider provider;

    public MarshallingDecoder(UnmarshallerProvider provider) {
        this(provider, 1048576);
    }

    public MarshallingDecoder(UnmarshallerProvider provider, int maxObjectSize) {
        // ???????4??
        super(maxObjectSize, 0, 4, 0, 4);
        this.provider = provider;
    }

    protected Object decode(ChannelHandlerContext ctx, ByteBuf in) throws Exception {
        ByteBuf frame = (ByteBuf)super.decode(ctx, in);
        if (frame == null) {
            return null;
        } else {
            Unmarshaller unmarshaller = this.provider.getUnmarshaller(ctx);
            ChannelBufferByteInput input = new ChannelBufferByteInput(frame);

            Object var7;
            try {
                unmarshaller.start(input);
                Object obj = unmarshaller.readObject();
                unmarshaller.finish();
                var7 = obj;
            } finally {
                unmarshaller.close();
            }

            return var7;
        }
    }

    protected ByteBuf extractFrame(ChannelHandlerContext ctx, ByteBuf buffer, int index, int length) {
        return buffer.slice(index, length);
    }
}
```



### MarshallingEncoder ??
????MarshallingEncoder????????????4????LENGTH_PLACEHOLDER ??????????

```java
@Sharable
public class MarshallingEncoder extends MessageToByteEncoder<Object> {
    private static final byte[] LENGTH_PLACEHOLDER = new byte[4];
    private final MarshallerProvider provider;

    public MarshallingEncoder(MarshallerProvider provider) {
        this.provider = provider;
    }

    protected void encode(ChannelHandlerContext ctx, Object msg, ByteBuf out) throws Exception {
        Marshaller marshaller = this.provider.getMarshaller(ctx);
        int lengthPos = out.writerIndex();
        out.writeBytes(LENGTH_PLACEHOLDER);
        ChannelBufferByteOutput output = new ChannelBufferByteOutput(out);
        marshaller.start(output);
        marshaller.writeObject(msg);
        marshaller.finish();
        marshaller.close();
        out.setInt(lengthPos, out.writerIndex() - lengthPos - 4);
    }
}
```












