package com.daryl.mybatis.controller;

import com.daryl.mybatis.entity.User;
import com.daryl.mybatis.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/4/19 10:28
 */
@RestController
public class TestController {

    @Autowired
    CacheManager cacheManager;

//    @Resource
    @Autowired
    private UserService userService;

    @RequestMapping("/getUser/{id}")
    public User getUser(@PathVariable Long id) {
        return userService.getUser(id);
    }

    @RequestMapping("/getUser/{id}/{name}")
    public User getUser(@PathVariable Long id, @PathVariable String name) {
        return userService.getUser(id, name);
    }

    @RequestMapping("/getAll")
    public List<User> getAll() {
        return userService.getAll();
    }

    @RequestMapping("/getUserByName/{name}")
    public User getUserByName(@PathVariable String name) {
        System.out.println(cacheManager.toString());
        System.out.println("------------------------------");
        return userService.getUserByName(name);
    }

    @GetMapping("/adduser")
    public int addSser() {
        User user = new User();
        user.setId(111L);
        user.setUsername("赵四");
        user.setPassword("1234");
        return userService.addUser(user);
    }

}
