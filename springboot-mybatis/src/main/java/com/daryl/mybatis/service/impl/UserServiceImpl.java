package com.daryl.mybatis.service.impl;

import com.daryl.mybatis.dao.UserMapper;
import com.daryl.mybatis.entity.User;
import com.daryl.mybatis.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/4/19 10:25
 */
@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserMapper userMapper;

    @Override
    public User getUser(Long id) {
        return userMapper.getUser(id);
    }

    @Override
    public List<User> getAll() {
        return userMapper.getAll();
    }

    @Override
    public User getUserByName(String name) {
        return userMapper.getUserByName(name);
    }

    @Override
    public User getUser(Long id, String name) {
        return userMapper.getUserByIdAndName(id, name);
    }

    @Override
    public int addUser(User user) {
        return userMapper.addUser(user);
    }
}
