package com.daryl.mybatis.service;

import com.daryl.mybatis.entity.User;

import java.util.List;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/4/19 10:25
 */
public interface UserService {

    User getUser(Long id);

    List<User> getAll();

    User getUserByName(String name);

    User getUser(Long id, String name);

    int addUser(User user);

}
