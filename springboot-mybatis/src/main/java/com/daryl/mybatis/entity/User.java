package com.daryl.mybatis.entity;

import lombok.Data;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/4/19 1:09
 */
@Data
public class User {

    private Long id;
    private String username;
    private String password;

}
