package com.daryl.mybatis.dao;

import com.daryl.mybatis.entity.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/4/19 10:23
 */
public interface UserMapper {

    @Select("select * from user where id = #{id}")
    @Results({@Result(property = "username", column = "user_name"), @Result(property = "password", column = "password")})
    User getUser(Long id);

    @Select("select * from user where id = #{id} and user_name=#{name}")
    User getUserByIdAndName(@Param("id") Long id, @Param("name") String username);

    @Select("select * from user")
    List<User> getAll();

    @Insert("insert into user(id, user_name, password) values(#{id}, #{username}, #{password})")
    int addUser(User user);

    /**
     * 使用xml方式
     * @param username
     * @return
     */
    User getUserByName(String username);
}
