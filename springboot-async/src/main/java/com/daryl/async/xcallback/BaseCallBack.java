package com.daryl.async.xcallback;

/**
 * 回调基类
 *
 * @author syj
 */
public abstract class BaseCallBack {
    /**
     * 回调执行逻辑
     *
     * @throws Exception
     */
    public abstract void run() throws Exception;
}
