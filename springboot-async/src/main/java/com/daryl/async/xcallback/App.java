package com.daryl.async.xcallback;

/**
 * 测试类
 *
 * @author syj
 */
public class App {
    // TCP 服务器IP
    public static String tcpSrvAddr = "127.0.0.1";
    // TCP 服务端口
    public static int tcpSrvPort = 9090;

    public static void main(String[] args) {
        Provider provider = new Provider();
        provider.initConfig(tcpSrvAddr, tcpSrvPort);
        provider.start();
        provider.stop();
    }
}
