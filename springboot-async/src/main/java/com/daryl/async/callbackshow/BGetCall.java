package com.daryl.async.callbackshow;

/**
 * @author cc
 * 接收回调事件的对象，处理完成回调接口，返回给发起回调的A
 * 02 创建被调用者B
 */
public class BGetCall {

    public void GetCall(ACallBack aCallBack, String from_a) {
        System.out.println("---A 调用 B的方法--传递参数是----" + from_a);

        for (int i = 0; i < 100000; i++) {
        }

        String backToA = "B响应回调，发送此消息到A,在A中输出";

        aCallBack.SpeakToA(backToA);
    }
}
