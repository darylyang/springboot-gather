package com.daryl.async.callbackshow;

/**
 * @author cc
 * 回调接口
 * 01 首先创建回调接口
 */
public interface ACallBack {

    /**
     * @param something 回调时返回的参数
     *                  调用的类在完成之后，回调A
     */
    void SpeakToA(String something);

}
