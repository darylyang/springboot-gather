package com.daryl.async.callbackshow;

/**
 * @author cc
 * <p>
 * A实现接口的方法等待 受理回调的类的返回
 * 03 创建调用者A  这里需要B对象调用B里边的方法，然后A中收到回调（即B调用A的方法，也就是从B返回到A的参数）
 */
public class ASendCall implements ACallBack {

    BGetCall bGetCall;

    public ASendCall(BGetCall b) {
        this.bGetCall = b;
    }

    /**
     * @param someToB 从这里A调用B的方法，把回调传过去。
     */
    public void CallToB(final String someToB) {
        new Thread(() -> {
            //用b的对象调用自己的方法
            bGetCall.GetCall(ASendCall.this, someToB);
        }).start();

        //上边调用了回调
        //这里可以处理一些等待回调返回是的数据

    }

    /* (non-Javadoc)
     * @see com.callback.ACallBack#SpeakToA(java.lang.String)
     * 接收回调信息的返回
     */
    @Override
    public void SpeakToA(String something) {
        System.out.println("这里的消息来自于B响应回调之后,内容是=====" + something);
    }

}
