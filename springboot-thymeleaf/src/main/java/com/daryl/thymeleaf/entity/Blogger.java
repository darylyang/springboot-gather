package com.daryl.thymeleaf.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/4/18 15:00
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Blogger {
    private Long id;
    private String name;
    private String pass;
}
