[TOC]

# Thymeleaf模板引擎

##  Thymeleaf 介绍
Thymeleaf 是适用于 Web 和独立环境的现代服务器端 Java 模板引擎。

Thymeleaf 的主要目标是为您的开发工作流程带来优雅的自然模板 - 可以在浏览器中正确显示的
HTML，也可以用作静态原型，从而在开发团队中实现更强大的协作。

传统的 JSP+JSTL 组合是已经过去了，Thymeleaf 是现代服务端的模板引擎，与传统的 JSP 不同，Thymeleaf 可以使用浏览器直接打开，因为可以忽略掉拓展性，相当于打开原生页面，给前端人员也带来一定的便利。


就是说在本地环境或者有网络的环境下，Thymeleaf 均可运行。由于 thymeleaf 支持html 原型，也支持在 html 标签里增加额外的属性来达到 “模板+数据” 的展示方式，所以美工可以直接在浏览器中查看页面效果，当服务启动后，也可以让后台开发人员查看带数据的动态页面效果。比如：

```
<div class="ui right aligned basic segment">
	<div class="ui orange basic label" th:text="${blog.flag}">静态原创信息</div>
</div>
<h2 class="ui center aligned header" th:text="${blog.title}">这是静态标题</h2>
```

在静态页面时，会展示静态信息，当服务启动后，动态获取数据库中的数据后，就可以展示动态数据， th:text 标签是用来动态替换文本的，这会在下文说明。该例子说明浏览器解释html 时会忽略 html 中未定义的标签属性（比如 th:text ），所以 thymeleaf 的模板可以静态地运行；当有数据返回到页面时，Thymeleaf 标签会动态地替换掉静态内容，使页面动态显示数据。


## 依赖导入
```pom
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-thymeleaf</artifactId>
</dependency>
```

在 html 页面上如果要使用 thymeleaf 模板，需要在页面标签中引入：
```html
<html xmlns:th="http://www.thymeleaf.org">
```

## Thymeleaf相关配置

因为 Thymeleaf 中已经有默认的配置了，我们不需要再对其做过多的配置，有一个需要注意一下，Thymeleaf 默认是开启页面缓存的，所以在开发的时候，需要关闭这个页面缓存，配置如下。
```yml
spring:
  thymeleaf:
    cache: false #关闭缓存
```
否则会有缓存，导致页面没法及时看到更新后的效果。 比如你修改了一个文件，已经 update 到tomcat 了，但刷新页面还是之前的页面，就是因为缓存引起的。


## Thymeleaf 的使用

### 访问静态页面
Spring Boot 中会自动识别模板目录（templates/）下的 404.html 和 500.html 文件。我们在templates/ 目录下新建一个 error 文件夹，专门放置错误的 html 页面，然后分别打印些信息。

404.html 为例：
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
    这是404页面
</body>
</html>
```

controller
```
@Controller
@RequestMapping("/thymeleaf")
public class ThymeleafController {

    @GetMapping("/test404")
    public String test404() {
        return "index";
    }

    @GetMapping("/test500")
    public String test500() {
        int i = 1 / 0;
        return "index";
    }
}
```

故意输入错误，找不到对应的方法，就会跳转到 404.html 显示。
当我们在浏览器中输入 localhost:8088/thymeleaf/test505 时，会抛出异常，然后会自动跳转到 500.html 显示。

### thymeleaf标签

| 标签         | 功能             | 例子                                                                              |
|------------|----------------|---------------------------------------------------------------------------------|
| th:value   | 给属性赋值          | <input th:value="$\{blog\.name\}" />                                            |
| th:style   | 设置样式           | th:style="'display:'\+@\{\($\{sitrue\}?'none':'inline\-block'\)\} \+ ''"        |
| th:onclick | 点击事件           | th:onclick="'getInfo\(\)'"                                                      |
| th:if      | 条件判断           | <a th:if="$\{userId == collect\.userId\}" >                                     |
| th:href    | 超链接            | <a th:href="@\{/blogger/login\}">Login</a> />                                   |
| th:unless  | 条件判断和 th:if 相反 | <a th:href="@\{/blogger/login\}"th:unless=$\{session\.user \!= null\}>Login</a> |
| th:switch  | 配合             | th:case <div th:switch="$\{user\.role\}">                                       |
| th:case    | 配合             | th:switch <p th:case="'admin'">administator</p>                                 |
| th:src     | 地址引入           | <img alt="csdn logo" th:src="@\{/img/logo\.png\}" />                            |
| th:action  | 表单提交的地址        | <form th:action="@\{/blogger/update\}">                                         |


详细看 官方 文档
[https://www.thymeleaf.org/doc/tutorials/3.0/usingthymeleaf.html](https://www.thymeleaf.org/doc/tutorials/3.0/usingthymeleaf.html)