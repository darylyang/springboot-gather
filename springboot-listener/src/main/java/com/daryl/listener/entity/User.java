package com.daryl.listener.entity;

import lombok.*;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/4/19 22:43
 */
@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class User {

    private Long id;

    private String username;

    private String password;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
