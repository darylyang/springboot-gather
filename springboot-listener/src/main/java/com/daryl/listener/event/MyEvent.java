package com.daryl.listener.event;

import com.daryl.listener.entity.User;
import org.springframework.context.ApplicationEvent;

/**
 * 自定义事件
 *
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/4/19 22:42
 */
public class MyEvent extends ApplicationEvent {

    private User user;

    public MyEvent(Object source, User user) {
        super(source);
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
