package com.daryl.annotation.entity;

import lombok.Data;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/4/18 14:02
 */
@Data
public class User {

    private String username;
    private String password;
}
