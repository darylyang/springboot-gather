
## Spring Boot返回Json数据及数据封装

### Spring Boot中默认使用的Json解析技术框架是jackson。
引入的 spring-boot-starter-web 依赖中，引入了spring-boot-starter-json 依赖。
而spring-boot-starter-json 引入的是 jackson


### @RestController注解
```java
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Controller
@ResponseBody
public @interface RestController {
    @AliasFor(
        annotation = Controller.class
    )
    String value() default "";
}
```

@RestController注解包含了原来的@Controller和QResponseBody注解
@ResponseBody注解是将返回的数据结构转换为Json格式。

所以在默认情况下，使用了@RestControl1er 注解即可将返回的数据结构转换成Json格式，


### jackson 中对null的处理
JacksonConfig配置类
```java
    @Bean
    @Primary
    @ConditionalOnMissingBean(ObjectMapper.class)
    public ObjectMapper jacksonObjectMapper(Jackson2ObjectMapperBuilder builder) {
        ObjectMapper objectMapper = builder.createXmlMapper(false).build();
        objectMapper.getSerializerProvider().setNullValueSerializer(new JsonSerializer<Object>() {
            @Override
            public void serialize(Object o, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
                jsonGenerator.writeString("");
            }
        });
        return objectMapper;
    }
```

### jackson 和 fastJson 的对比

| 选项       | fastjson | jackson |
|----------|----------|---------|
| 上手难易     | 容易       | 中等      |
| 高级特性     | 中等       | 丰富      |
| 处理json速度 | 略快       | 快       |

### 使用 fastJson 处理 null
使用 fastJson 时，对 null 的处理需要继承WebMvcConfigurationSupport
类，然后覆盖 configureMessageConverters 方法，在方法中，我们可以选择对要实现 null 转换的场景配置好即可。

```java
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        FastJsonHttpMessageConverter converter = new FastJsonHttpMessageConverter();
        com.alibaba.fastjson.support.config.FastJsonConfig config = new com.alibaba.fastjson.support.config.FastJsonConfig();
        config.setSerializerFeatures(
                // 保留map空的字段
                SerializerFeature.WriteMapNullValue,
                // 将String类型的null转成""
                SerializerFeature.WriteNullStringAsEmpty,
                // 将Number类型的null转成0
                SerializerFeature.WriteNullNumberAsZero,
                // 将List类型的null转成[]
                SerializerFeature.WriteNullListAsEmpty,
                // 将Boolean类型的null转成false
                SerializerFeature.WriteNullBooleanAsFalse,
                // 避免循环引用
                SerializerFeature.DisableCircularReferenceDetect);

        converter.setFastJsonConfig(config);
        converter.setDefaultCharset(Charset.forName("UTF-8"));
        List<MediaType> mediaTypeList = new ArrayList<>();
        // 解决中文乱码问题，相当于在Controller上的@RequestMapping中加了个属性produces = "application/json"
        mediaTypeList.add(MediaType.APPLICATION_JSON);
        converter.setSupportedMediaTypes(mediaTypeList);
        converters.add(converter);
    }
```

### 封装统一返回的数据结构

定义统一的 json 结构
修改 Controller 中的返回值类型及测试
