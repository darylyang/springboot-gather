package com.daryl.json.tools;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.text.StringEscapeUtils;
import org.junit.Test;

/**
 * json串 去掉反斜杠 即去掉转义符 escape
 *
 * * <p>#ThreadSafe#</p>
 * * @since 2.0
 * * @deprecated as of 3.6, use commons-text
 * * <a href="https://commons.apache.org/proper/commons-text/javadocs/api-release/org/apache/commons/text/StringEscapeUtils.html">
 * * StringEscapeUtils</a> instead
 *
 * <dependency>
 *     <groupId>org.apache.commons</groupId>
 *     <artifactId>commons-text</artifactId>
 *     <version>1.8</version>
 * </dependency>
 *
 * Apache 的 common.lang3 工具包(使用工具类：org.apache.commons.lang3.StringEscapeUtils) 过时
 * * <p>Escapes and unescapes {@code String}s for
 * * Java, Java Script, HTML and XML.</p>
 * *
 *
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/4/13 0:36
 */
public class StringEscapeTest {

    @Test
    public void testJsonEscape() {
        String jsonStr = "{\"resourceId\":\"dfead70e4ec5c11e43514000ced0cdcaf\",\"properties\":{\"process_id\":\"process4\"," + "\"name\":\"\"," +
                "\"documentation\":\"\",\"processformtemplate\":\"\"}}";

        System.out.println(JSONObject.toJSONString(jsonStr));

        String newStr = StringEscapeUtils.unescapeJson(jsonStr);
        System.out.println(newStr);
    }
}
