package com.daryl.json;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/4/12 23:11
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class MessageData {

    @JSONField(name = "Cmd")
    private int cmd;
    @JSONField(name = "Cmd_desc")
    private String cmdDesc;
    @JSONField(name = "Event")
    private String event;
    @JSONField(name = "TimeSpan")
    private String timespan;
    @JSONField(name = "D_type")
    private String dType;
    @JSONField(name = "C_Type")
    private String cType;
    @JSONField(name = "AREA_ID")
    private String areaCode;
    @JSONField(name = "CHNL_NO")
    private String channelCode;
    @JSONField(name = "I_E_TYPE")
    private String ieType;
    @JSONField(name = "SEQ_NO")
    private String seqNo;
    @JSONField(name = "Data")
    private Object data;
}
