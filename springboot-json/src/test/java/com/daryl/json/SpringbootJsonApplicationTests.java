package com.daryl.json;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.daryl.json.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class SpringbootJsonApplicationTests {

    @Test
    void contextLoads() {
    }

    @Test
    public void testFastjson() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("111", "AAA");
        jsonObject.put("222", "BBB");
        jsonObject.put("333", "CCC");
        JSONObject json = new JSONObject();
        json.put("444", "aaa");
        json.put("555", "bbb");
        json.put("666", "ccc");
        jsonObject.put("777", json);

        System.out.println(jsonObject.toJSONString());

        System.out.println(JSONObject.toJSONString(""));
        System.out.println(JSONObject.toJSONString(null));
        System.out.println(JSONObject.toJSONString(null, true));
        System.out.println(JSONObject.toJSONString(null, SerializerFeature.WriteNullStringAsEmpty));

        Model build = Model.builder().id(12).build();
        System.out.println(build);
        System.out.println(JSONObject.toJSONString(build));

    }

    @Test
    public void testJson () {
        String jsonMsg = "{'Cmd':203,'Cmd_desc':'0xCB','Event':'COMMAND_INFO','TimeSpan':'2020-03-27 14:40:35','D_type':'JSON','C_Type':'PLAT_FORM','AREA_ID':'0755000001','CHNL_NO':'8616918601','I_E_TYPE':'I','SEQ_NO':'00000000000000000000','Data':{'ReleaseVer':'1585291235985','FtpDir':'/workFlow/','FtpIP':'172.18.11.226','FtpUser':'win7','FtpPw':'qwerqwer','FileNumber':'4','FileUpdate':[{'FileName':'workflow_8616918601.xml','FileVer':''},{'FileName':'8616918601_1585291236386.xml','FileVer':''},{'FileName':'workflows.xml','FileVer':''},{'FileName':'JSONDATA_8616918601.json','FileVer':''}]}}";
        // jsonStr 转 bean对象
        MessageData messageData = JSONObject.parseObject(jsonMsg, MessageData.class);
        if (messageData == null) {
            return;
        }
        User user1 = User.builder().id(1).username("aaa").password("aaa").build();
        User user2 = User.builder().id(2).username("bbb").password("bbb").build();
        User user3 = User.builder().id(3).username("ccc").password("ccc").build();

        List<User> userList = new ArrayList<>();
        userList.add(user1);
        userList.add(user2);
        userList.add(user3);
        String jsonStr = JSONObject.toJSONString(userList);
        messageData.setData(jsonStr);


        // bean对象 转 jsonStr
        String jsonString = JSONObject.toJSONString(messageData);
        System.out.println(jsonString);
    }

    @Test
    public void testJson2() {
        Model model = new Model();
        model.id = 1001;
        model.value = "{}";
        String json = JSON.toJSONString(model);
        System.out.println(json);
    }

}
