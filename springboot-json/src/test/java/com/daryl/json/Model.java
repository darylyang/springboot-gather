package com.daryl.json;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.*;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/4/12 23:36
 */
@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Model {

    public int id;
    @JSONField(jsonDirect=true)
    public String value;
}
