package com.daryl.json.entity;

import lombok.*;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/4/8 23:07
 */
@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class User {

    private Integer id;
    private String username;
    private String password;
}
