package com.daryl.json.controller;

import com.daryl.json.entity.JsonResult;
import com.daryl.json.entity.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/4/9 21:21
 */
@RestController
@RequestMapping("/jsonresult")
public class JsonResultController {

    @RequestMapping("/user")
    public JsonResult<User> getUser() {
        User user = new User(1, "daryl", "123456");
        return new JsonResult<>(user);
    }

    @RequestMapping("/list")
    public JsonResult<List<User>> getUserList() {
        List<User> userList = new ArrayList<>();

        User user1 = new User(1, "stduy", "123456");
        User user2 = new User(2, "fight", "123456");
        User user3 = new User(3, "fight", null);
        userList.add(user1);
        userList.add(user2);
        userList.add(user3);
        userList.add(new User());
        return new JsonResult<>(userList, "获取用户列表成功");
    }

    @RequestMapping("/map")
    public JsonResult<Map<String, Object>> getMap() {
        Map<String, Object> map = new HashMap<>(3);

        User user = new User(1, "daryl", null);
        map.put("作者信息", user);
        map.put("博客地址", "https://blog.csdn.net/");
        map.put("CSDN地址", null);
        map.put("粉丝数量", 1243);
        return new JsonResult<>(map);
    }
}