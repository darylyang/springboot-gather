# springboot-gather

#### 介绍
springboot整合各个框架

springboot-ftp 运用apache-common-pool2 创建了ftp连接池


- [ ] springboot-actuator
- [ ] springboot-annotation
- [ ] springboot-aop
- [ ] springboot-collection
- [ ] springboot-concurrent
- [ ] springboot-demo1
- [ ] springboot-docker
- [ ] springboot-easyexcel
- [ ] springboot-ehcache
- [ ] springboot-excel
- [ ] springboot-exception
- [ ] springboot-ftp
- [ ] springboot-gather.iml
- [ ] springboot-guava
- [ ] springboot-hutool
- [ ] springboot-interceptor
- [ ] springboot-jdk8
- [ ] springboot-job-core
- [ ] springboot-json
- [ ] springboot-listener
- [ ] springboot-log
- [ ] springboot-lucene
- [ ] springboot-mail
- [ ] springboot-mq
- [ ] springboot-mybatis
- [ ] springboot-netty
- [ ] springboot-oshi
- [ ] springboot-oss
- [ ] springboot-properties
- [x] springboot-rabbitmq
- [ ] springboot-redis
- [ ] springboot-shiro
- [ ] springboot-socket
- [ ] springboot-swagger
- [ ] springboot-test
- [ ] springboot-threadpool
- [ ] springboot-thymeleaf
- [ ] springboot-tools
- [ ] springboot-transaction

