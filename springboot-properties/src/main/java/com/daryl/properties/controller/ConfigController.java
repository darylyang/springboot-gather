package com.daryl.properties.controller;

import com.daryl.properties.config.MicroServiceUrl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/4/18 11:12
 */
@Slf4j
@RestController
@RequestMapping("/test")
public class ConfigController {

    @Value("${url.orderUrl}")
    private String orderUrl;

    @Resource
    private MicroServiceUrl microServiceUrl;

    @RequestMapping("/config")
    public String testConfig() {
        log.info("=====获取的订单服务地址为：{}", orderUrl);
        // 使用配置类来获取
        log.info("=====获取的订单服务地址为：{}", microServiceUrl.getOrderUrl());
        log.info("=====获取的用户服务地址为：{}", microServiceUrl.getUserUrl());
        log.info("=====获取的购物车服务地址为：{}", microServiceUrl.getShoppingUrl());
        return "success";
    }
}
