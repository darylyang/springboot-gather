package com.daryl.tools.fun;

import java.util.Arrays;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/4/21 21:23
 */
public class ArrayCopy {

    public static void main(String[] args) {
        int[] score = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int[] score2 = new int[5];
        System.arraycopy(score, 2, score2, 0, 3);
        System.out.println(Arrays.toString(score2));
    }
}
