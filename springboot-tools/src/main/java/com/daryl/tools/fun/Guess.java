package com.daryl.tools.fun;

import java.util.Scanner;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/4/21 20:56
 */
public class Guess {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = (int) (Math.random() * 10);
        int guess;

        do{
            System.out.println("猜数字（0-9）：");
            guess = scanner.nextInt();
        } while (guess != number);

        System.out.println("猜中了！！guess:" + guess);
    }
}
