package com.daryl.tools.guava;

import com.google.common.base.Optional;
import org.testng.annotations.Test;


/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/4/14 7:42
 */
public class OptionalTest {

    @Test
    public void testOptional1(){
        Optional<Integer> possible = Optional.of(5);
        // returns true
        System.out.println(possible.isPresent());
        // returns 5
        System.out.println(possible.get());
    }
}
