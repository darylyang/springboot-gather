package com.daryl.tools;

import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.IOException;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/4/7 21:23
 */
public class FileTest {


    @Test
    public void testResoourceFile() throws IOException {
        File file = new ClassPathResource("img/Pikachu.png").getFile();
        System.out.println(file.getPath());
        File file1 = ResourceUtils.getFile("classpath:img/Pikachu.png");
        System.out.println(file1.getPath());

        System.out.println(new ClassPathResource("img").getPath());
        System.out.println(new ClassPathResource("img").getFile().getName());
        System.out.println(new ClassPathResource("img").getFile().getPath());
        System.out.println(new ClassPathResource("img").getFilename());

    }

    @Test
    public void test00() {
        boolean deleteFlag = FileUtils.deleteFile("E:\\aa\\txt\\00.txt");
        System.out.println("deleteFlag:" + deleteFlag);

        boolean validFilename = FileUtils.isValidFilename("vhead_14-54-02(粤A-BB333) - 副本.jpg");
        System.out.println("validFilename" + validFilename);

    }

    @Test
    public void test01() {
        //相对路径，如果没有前面的src，就在当前目录创建文件
        File file = new File("E:\\aa\\txt\\00.txt");
        if (file.exists()) {
            System.out.println("文件已经存在");
        } else {
            try {
                file.createNewFile();
                System.out.println("文件创建成功");
            } catch (Exception e) {
            }
        }
        System.out.println("文件已经存在:" + file.exists());
        System.out.println("文件的名字:" + file.getName());
        System.out.println("文件的路径:" + file.getPath());
        System.out.println("文件的绝对路径:" + file.getAbsolutePath());
        System.out.println("是目录吗:" + file.isDirectory());
        System.out.println("文件大小:" + file.length());
    }
}
