package com.daryl.threadpool.core;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

/**
 * @ClassName: Task
 * @Description: 模拟的任务
 */
@Slf4j
public class Task {

    private String name;

    public Task(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Task[name=" + name + "]";
    }

    public void doSomething() {
        long sleepTime = new Double(Math.random() * 10).longValue();
        try {
            //模拟任务的执行时间
            log.info("{} doing my task|{}, I need execute {} s", Thread.currentThread().getName(), this, sleepTime);
            TimeUnit.SECONDS.sleep(sleepTime);
        } catch (InterruptedException e) {
            log.error("{} execute task|{} error", Thread.currentThread().getName(), this, e);
        }
        log.info("{} done my task|{}", Thread.currentThread().getName(), this);
    }
}
