package com.daryl.threadpool.core;

/**
 * @ClassName: Pool
 * @Description: 线程池接口
 */
public interface Pool<T> {
    /**
     * 关闭线程池，会等待未完成任务的线程
     */
    void shutdown();

    /**
     * 立即关闭线程池，不会等待线程完成
     */
    void shutdownnow();

    /**
     * 执行任务
     *
     * @param task
     */
    void execute(Task task);

    /**
     * 借
     *
     * @return
     */
    T borrowFromPool();

    /**
     * 还
     *
     * @param t
     */
    void returnToPool(T t);

    /**
     * 得到线程池关闭标志
     *
     * @return
     */
    boolean isShutdown();

    /**
     * 设置线程池关闭标志
     *
     * @param isShutdown
     */
    void setShutdown(boolean isShutdown);

}
