package com.daryl.threadpool.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Future;

/**
 * 使用连接池
 * 在业务方法中使用@Async注解，并且可以选择使用的连接池。来启动一个异步任务。
 *
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/6/4 22:40
 */
@Slf4j
@Component
public class ServiceImpl {

    //带返回值的任务
    @Async("asyncServiceExecutor")
    public Future<String> doTask1() throws InterruptedException {
        log.info("Task1 started.");
        long start = System.currentTimeMillis();
        Thread.sleep(5000);
        long end = System.currentTimeMillis();

        log.info("Task1 finished, time elapsed: {} ms.", end - start);

        return new AsyncResult<>("Task1 accomplished!");
    }

    @Async("customServiceExecutor")
    public Future<String> doTask2() throws InterruptedException {
        log.info("Task2 started.");
        long start = System.currentTimeMillis();
        Thread.sleep(3000);
        long end = System.currentTimeMillis();

        log.info("Task2 finished, time elapsed: {} ms.", end - start);

        return new AsyncResult<>("Task2 accomplished!");
    }

    //带返回值的任务
    @Async("asyncServiceExecutor")
    public Future<String> doTask1(CountDownLatch latch) throws InterruptedException {
        log.info("Task1 started.");
        long start = System.currentTimeMillis();
        Thread.sleep(5000);
        long end = System.currentTimeMillis();

        log.info("Task1 finished, time elapsed: {} ms.", end - start);
        latch.countDown();
        return new AsyncResult<>("Task1 accomplished!");
    }

    @Async("customServiceExecutor")
    public Future<String> doTask2(CountDownLatch latch) throws InterruptedException {
        log.info("Task2 started.");
        long start = System.currentTimeMillis();
        Thread.sleep(3000);
        long end = System.currentTimeMillis();

        log.info("Task2 finished, time elapsed: {} ms.", end - start);
        latch.countDown();
        return new AsyncResult<>("Task2 accomplished!");
    }

    //创建的是Runnable的任务
    @Async("asyncServiceExecutor")
    public void executeAsync() {
        log.info("start executeAsync");
        try {
            Thread.sleep(1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.info("end executeAsync");
    }

    @Async("asyncServiceExecutor")
    public void executeAsync2() {
        log.info("start executeAsync");
        int i = 1 / 0;
        try {
            Thread.sleep(1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.info("end executeAsync");
    }

//    public void sendMail(Map<String, Object> model, String title, String templateName, String toMail, String[] ccMail, long timeout) throws Exception {
//        Future<String> submit;
//        submit = emailServiceExecutor.submit(() -> {
//            try {
//                return "s";
//            } catch (Exception e) {
//                return "F";
//            }
//        });
//    }

}
