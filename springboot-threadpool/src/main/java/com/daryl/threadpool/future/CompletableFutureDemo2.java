package com.daryl.threadpool.future;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/6/11 21:33
 */
public class CompletableFutureDemo2 {

    public static void main(String[] args) throws InterruptedException {
        Map<Integer, Integer> map= new HashMap<>();

        for (int i = 0; i < 6; i++) {
            map.put(i, i);
//            CompletableFuture<Boolean> future = CompletableFuture.supplyAsync()
        }


        long l = System.currentTimeMillis();
        CompletableFuture<Integer> completableFuture = CompletableFuture.supplyAsync(() -> {
            System.out.println("在回调中执行耗时操作...");
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return 100;
        });
        completableFuture = completableFuture.thenCompose(i -> {
            return CompletableFuture.supplyAsync(() -> {
                System.out.println("在回调的回调中执行耗时操作...");
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return i + 100;
            });
        });
        completableFuture.whenComplete((result, e) -> {
            System.out.println("计算结果:" + result);
        });
        System.out.println("主线程运算耗时:" + (System.currentTimeMillis() - l) + " ms");
        new CountDownLatch(1).await();
    }
}
