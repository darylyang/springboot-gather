package com.daryl.threadpool.future.demo1;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/6/11 21:56
 */
public interface Listener {

    void onSuccess(Object... args);

    void onFailure(Throwable cause);
}
