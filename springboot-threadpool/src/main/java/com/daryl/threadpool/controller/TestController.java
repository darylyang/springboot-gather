package com.daryl.threadpool.controller;

import com.alibaba.fastjson.JSON;
import com.daryl.threadpool.service.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/6/4 22:41
 */
@Slf4j
@Controller
public class TestController {

    @Autowired
    ServiceImpl serviceImpl;

    /**
     * 使用自旋操作，等待任务结果返回。
     * @return
     */
    @RequestMapping("/helloFuture")
    @ResponseBody
    public String helloFuture() {
        try {
            Future<String> future1 = serviceImpl.doTask1();
            Future<String> future2 = serviceImpl.doTask2();
            //自旋锁，停止等待
            while (true) {
                if (future1.isDone() && future2.isDone()) {
                    log.info("Task1 result:{}", future1.get());
                    log.info("Task2 result:{}", future2.get());
                    break;
                }
                Thread.sleep(1000);
            }
            log.info("All tasks finished.");
            return "S";
        } catch (InterruptedException e) {
            log.error("错误信息1", e);
            return "F";
        } catch (ExecutionException e) {
            log.error("错误信息2", e);
            return "F";
        }
    }

    /**
     * 使用CountDownLatch计数器  每个任务执行完毕，只需要调用latch.countDown();使得计数器-1。
     * @return
     */
    @RequestMapping("/helloFuture2")
    @ResponseBody
    public String helloFuture2() {
        try {
            CountDownLatch latch = new CountDownLatch(2);
            Future<String> future1 = serviceImpl.doTask1(latch);
            Future<String> future2 = serviceImpl.doTask2(latch);
            //等待两个线程执行完毕
            latch.await();
            log.info("All tasks finished!");
            String result1 = future1.get();
            String result2 = future2.get();
            log.info(result1 + "--" + result2);
            return "S";
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return "F";
    }

    /**
     * 使用Future的get方法的阻塞特性
     * @return
     */
    @RequestMapping("/helloFuture3")
    @ResponseBody
    public String helloFuture3() {
        try {
            List<Future<String>> tasks = new ArrayList<>();
            List<String> results = new ArrayList<>();
            tasks.add(serviceImpl.doTask1());
            tasks.add(serviceImpl.doTask2());
            //各个任务执行完毕
            for (Future<String> task : tasks) {
                //每个任务都会再在此阻塞。
                results.add(task.get());
            }
            log.info("All tasks finished!");
            log.info("执行结果：{}", JSON.toJSONString(results));
            return "S";
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return "F";
    }
}
