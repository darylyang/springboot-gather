package com.daryl.threadpool.controller;

import com.daryl.threadpool.config.ThreadConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.concurrent.CountDownLatch;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/6/4 22:03
 */
@Slf4j
//@Controller
public class TempController {

//    @Resource
    private ThreadConfig threadConfig;

    @RequestMapping(value = "/multThread", method = RequestMethod.GET)
    @ResponseBody
    public String multThread(String index) {
        CountDownLatch countDownLatch = new CountDownLatch(4);
        for (int xx = 0; xx < 4; xx++) {
            threadConfig.asyncServiceExecutor().execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        String result = "A";
                        log.info("线程--> " + index + " 开始");
                        for (long i = 0; i < 99999L; i++) {
                            result += "A";
                        }
                        //                        log.info(result);
                        log.info("线程--> " + index + " 结束");
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        Long l = countDownLatch.getCount();
                        //            log.info("current threads --> " + l);
                        countDownLatch.countDown();  // 这个不管是否异常都需要数量减,否则会被堵塞无法结束
                    }
                }
            });
        }
        try {
            countDownLatch.await();
            log.info(index + "所有线程结束");
        } catch (Exception e) {
            log.error("阻塞异常");
        }
        return index;
    }

}
