
[TOC]

# springboot log

Spring Boot在所有内部⽇志中使⽤Commons Logging，但是默认配置也提供了对常⽤⽇志的⽀持， 如：Java Util Logging，Log4J, Log4J2和Logback。每种Logger都可以通过配置使⽤控制台或者⽂件 输出⽇志内容。

## SLF4J
SLF4J，即简单日志门面（Simple Logging Facade for Java），不是具体的日志解决方案，它只服务于各种各样的日志系统。按照官方的说法，SLF4J是一个用于日志系统的简单Facade，允许最终用户在部署其应用时使用其所希望的日志系统。

《阿里巴巴Java开发手册(正式版)》中，日志规约一项第一条就强制要求使用 slf4j：
> 1.【强制】应用中不可直接使用日志系统（Log4j、Logback）中的API，而应依赖使用日志框架SLF4J中的API，使用门面模式的日志框架，有利于维护和各个类的日志处理方式统一。

```java
private static final Logger logger = LoggerFactory.getLogger(Test.class);
```

使用lombok插件
`@Slf4j`


## 格式化日志
```log
2020-04-14 22:33:38.177  INFO 3528 --- [  restartedMain] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat initialized with port(s): 8080 (http)
```

输出内容元素具体如下： 
* 时间⽇期： 精确到毫秒 
* ⽇志级别： ERROR, WARN, INFO, DEBUG or TRACE 
* 进程ID 
* 分隔符： --- 标识实际⽇志的开始 
* 线程名： ⽅括号括起来（可能会截断控制台输出） 
* Logger名： 通常使⽤源代码的类名 
* ⽇志内容

## 控制台输出
在Spring Boot中默认配置了 ERROR 、 WARN 和 INFO 级别的⽇志输出到控制台。 
我们可以通过两种⽅式切换⾄ DEBUG 级别：
* 在运⾏命令后加⼊ --debug 标志，如： $ java -jar myapp.jar --debug
* 在 application.properties 中配置 `debug=true`, 该属性置为true的时候，核⼼Logger（包含嵌入式容器、hibernate、spring）会输出更多内容，但是你⾃⼰应⽤的⽇志并不会输出为DEBUG级 别。