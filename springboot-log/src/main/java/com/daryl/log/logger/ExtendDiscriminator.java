package com.daryl.log.logger;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.sift.AbstractDiscriminator;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/4/15 0:37
 */
@Slf4j
public class ExtendDiscriminator extends AbstractDiscriminator<ILoggingEvent> {

    private static final String KEY = "logDir";
    private String defaultLogDir;
    private Map<String, String> channelMap = new HashMap<>();

    public ExtendDiscriminator() {
        channelMap.put("aaaa", "cccc");
        channelMap.put("bbbb", "dddd");
        this.channelMap = channelMap;
    }

    @Override
    public String getDiscriminatingValue(ILoggingEvent event) {
        String message = event.getMessage();
//        log.info("message:{}", event.getMessage());
        String logDir = defaultLogDir;
        if (message.contains("aaaa")) {
            logDir = "aaaa" + File.separator + "cccc";
        } else if (message.contains("bbbb")) {
            logDir = "bbbb" + File.separator + "dddd";
        }
        return logDir;
    }

    @Override
    public String getKey() {
        return KEY;
    }

    public static String getKEY() {
        return KEY;
    }

    public String getDefaultLogDir() {
        return defaultLogDir;
    }

    public void setDefaultLogDir(String defaultLogDir) {
        this.defaultLogDir = defaultLogDir;
    }
}
