package com.daryl.log.logger;

import ch.qos.logback.core.PropertyDefinerBase;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/4/15 1:21
 */
public class LogbackProperty extends PropertyDefinerBase {
    private String home;

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    @Override
    public String getPropertyValue() {
        return home;
    }
}
