package com.daryl.log.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/test")
public class TestController {


    @RequestMapping("/log")
    public String testLog() {
        log.debug("=====测试日志debug级别打印====");
        log.info("======测试日志info级别打印=====");
        log.error("=====测试日志error级别打印====");
        log.warn("======测试日志warn级别打印=====");

        // 可以使用占位符打印出一些参数信息
        String str1 = "111";
        String str2 = "aaaa";
        log.info("======qq:{}; bbbb:{}", str1, str2);
        log.info("======qq:{}; aaaa:{}", str1, str2);
        try {
            int i = 1/0;
        } catch (Exception e) {
            log.error("aaaaaaa", e);
        }

        return "success";
    }
}
