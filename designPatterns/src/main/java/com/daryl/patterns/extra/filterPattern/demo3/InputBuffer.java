package com.daryl.patterns.extra.filterPattern.demo3;

public interface InputBuffer {
    int doRead(byte [] chunk) throws Exception;
}
