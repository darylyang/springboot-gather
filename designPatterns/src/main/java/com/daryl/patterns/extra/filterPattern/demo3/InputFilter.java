package com.daryl.patterns.extra.filterPattern.demo3;

public interface InputFilter extends InputBuffer {
    void setBuffer(InputBuffer inputBuffer);
}
