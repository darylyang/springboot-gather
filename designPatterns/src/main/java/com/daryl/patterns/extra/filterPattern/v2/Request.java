package com.daryl.patterns.extra.filterPattern.v2;

import lombok.Data;

/**
 * 请求类，模拟一次请求
 * 有名字和信息两个属性，根据实际情况自定义
 *
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/9/22 22:32
 */
@Data
public class Request {
    private String name;
    private String info = "";

    public Request(String name, int num) {
        this.name = name;
        this.num = num;
    }

    //一个条件数值，用来过滤条件判断
    private int num;
}
