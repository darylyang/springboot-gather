package com.daryl.patterns.extra.filterPattern.v2;

import java.util.ArrayList;
import java.util.List;

/**
 * 过滤器容器：存储多个过滤器，可以自定义执行顺序和数量，并使请求执行过滤器列表
 *
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/9/22 22:51
 */
public class FilterContainer {

    private List<Filter> list = new ArrayList<>();

    //设置要执行的过滤器列表
    public void setFilterList(List<Filter> list) {
        this.list = list;
    }

    //执行过滤器
    public void doFilter(Request request) {
        for (Filter filter : list) {
            //如果返回false,结束执行
            if (!filter.execute(request)) {
                break;
            }
        }
        System.out.println("执行完成！");
    }

    public static void main(String[] args) {
        //配置过滤器执行列表
        List<Filter> list = new ArrayList<>();
        list.add(new Filter_A());
        list.add(new Filter_B());
        list.add(new Filter_C());
        //加载过滤器容器
        FilterContainer filterContainer = new FilterContainer();
        filterContainer.setFilterList(list);
        //请求
        Request request1 = new Request("请求1", 25);
        Request request2 = new Request("请求2", 35);
        filterContainer.doFilter(request1);
        System.out.println(request1.getInfo());
        filterContainer.doFilter(request2);
        System.out.println(request2.getInfo());
    }
}
