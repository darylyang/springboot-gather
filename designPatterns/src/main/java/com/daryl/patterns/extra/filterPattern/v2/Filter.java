package com.daryl.patterns.extra.filterPattern.v2;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/9/22 22:49
 */
public interface Filter {

    /**
     * 对请求进行一次过滤或者处理
     * 如果继续执行后面的过滤器，返回true，否则返回false
     *
     * @param request 请求类
     */
    boolean execute(Request request);

}
