package com.daryl.patterns.extra.filterPattern.v1;

import java.util.List;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/9/22 23:39
 */
public interface Criteria {
    List<Person> meetCriteria(List<Person> persons);
}
