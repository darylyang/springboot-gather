package com.daryl.patterns.extra.filterPattern.v2;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/9/22 22:50
 */
public class Filter_A implements Filter {
    @Override
    public boolean execute(Request request) {
        System.out.println("请求" + request.getName() + "正在执行Filter_A过滤器-----------------------");
        //对请求进行判断
        if (request != null && request.getNum() > 10) {
            System.out.println(request.getInfo() + "通过了Filter_A过滤器,继续执行下一个");
            request.setInfo(request.getInfo() + "通过了Filter_A过滤器 ");
            return true;
        } else if (request != null) {
            request.setInfo(request.getInfo() + "未通过了Filter_A过滤器,停止执行\n");
        }
        return false;
    }

}
