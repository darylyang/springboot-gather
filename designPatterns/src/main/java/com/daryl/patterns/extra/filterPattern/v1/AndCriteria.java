package com.daryl.patterns.extra.filterPattern.v1;

import java.util.List;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/9/22 23:40
 */
public class AndCriteria implements Criteria {

    private Criteria criteria;
    private Criteria otherCriteria;

    public AndCriteria(Criteria criteria, Criteria otherCriteria) {
        this.criteria = criteria;
        this.otherCriteria = otherCriteria;
    }

    @Override
    public List<Person> meetCriteria(List<Person> persons) {
        // 先过滤第一个条件
        List<Person> firstCriteriaPersons = criteria.meetCriteria(persons);
        // 再过滤第二个条件
        return otherCriteria.meetCriteria(firstCriteriaPersons);
    }
}