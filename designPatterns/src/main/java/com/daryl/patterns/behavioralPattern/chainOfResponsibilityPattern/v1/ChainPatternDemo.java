package com.daryl.patterns.behavioralPattern.chainOfResponsibilityPattern.v1;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/9/23 1:02
 */
public class ChainPatternDemo {

    private static AbstractLogger getChainOfLoggers() {

        AbstractLogger errorLogger = new ErrorLogger(AbstractLogger.ERROR);
        AbstractLogger fileLogger = new FileLogger(AbstractLogger.DEBUG);
        AbstractLogger consoleLogger = new ConsoleLogger(AbstractLogger.INFO);

        errorLogger.setNextLogger(fileLogger);
        fileLogger.setNextLogger(consoleLogger);

        return errorLogger;
    }

    public static void main(String[] args) {
        AbstractLogger loggerChain = getChainOfLoggers();

        loggerChain.logMessage(AbstractLogger.INFO, "This is an information.");

        loggerChain.logMessage(AbstractLogger.DEBUG, "This is a debug level information.");

        loggerChain.logMessage(AbstractLogger.ERROR, "This is an error information.");
    }
}
//    Standard Console::Logger: This is an information.
//    File::Logger: This is a debug level information.
//    Standard Console::Logger: This is a debug level information.
//    Error Console::Logger: This is an error information.
//    File::Logger: This is an error information.
//    Standard Console::Logger: This is an error information.