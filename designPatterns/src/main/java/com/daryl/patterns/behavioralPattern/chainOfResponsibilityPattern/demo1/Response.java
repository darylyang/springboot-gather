package com.daryl.patterns.behavioralPattern.chainOfResponsibilityPattern.demo1;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: 瓦力
 * @date: 2020-08-17 23:49
 * @Target: 责任链模式
 **/
@Data
public class Response {
    private String responseBody;

    private Map<String, String> header = new HashMap<>();

}
