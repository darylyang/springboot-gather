package com.daryl.patterns.behavioralPattern.chainOfResponsibilityPattern.v1;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/9/23 1:01
 */
public class ErrorLogger extends AbstractLogger {

    public ErrorLogger(int level){
        this.level = level;
    }

    @Override
    protected void write(String message) {
        System.out.println("Error Console::Logger: " + message);
    }
}
