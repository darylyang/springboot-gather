package com.daryl.patterns.behavioralPattern.chainOfResponsibilityPattern.v1;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/9/23 1:01
 */
public class FileLogger extends AbstractLogger {

    public FileLogger(int level){
        this.level = level;
    }

    @Override
    protected void write(String message) {
        System.out.println("File::Logger: " + message);
    }
}

