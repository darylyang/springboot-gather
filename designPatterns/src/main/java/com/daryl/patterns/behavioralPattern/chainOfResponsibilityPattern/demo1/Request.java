package com.daryl.patterns.behavioralPattern.chainOfResponsibilityPattern.demo1;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: 瓦力
 * @date: 2020-08-17 23:48
 * @Target: 责任链模式
 **/
@Data
public class Request {
    private String requestBody;
    private Map<String, String> header = new HashMap<>();

}
