package com.daryl.patterns.behavioralPattern.chainOfResponsibilityPattern.v1;

/**
 * 创建抽象的记录器类。
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/9/23 1:00
 */
public abstract class AbstractLogger {
    public static int INFO = 1;
    public static int DEBUG = 2;
    public static int ERROR = 3;

    protected int level;

    //责任链中的下一个元素
    protected AbstractLogger nextLogger;

    public void setNextLogger(AbstractLogger nextLogger){
        this.nextLogger = nextLogger;
    }

    public void logMessage(int level, String message){
        if(this.level <= level){
            write(message);
        }
        if(nextLogger !=null){
            nextLogger.logMessage(level, message);
        }
    }

    abstract protected void write(String message);

}

