package com.daryl.patterns.creationalPattern.singletonPattern;

/**
 * @ 懒汉式单例
 * 懒汉式单例在第一次调用getInstance()方法时实例化，在类加载时并不自 行实例化，这种技术又称为延迟加载(Lazy Load)技术，即需要的时候再加载实例，
 * 为了避免 多个线程同时调用getInstance()方法，我们可以使用关键字synchronized
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2019/8/1 23:39
 */
public class LazySingleton {

    private static LazySingleton instance = null;

    private LazySingleton() {
    }

    /**
     * 解决线程安全问题
     * 但是 每次调用getInstance()时 都需要进行线程锁定判断，在多线程高并发访问环境中，将会导致系统性能大大降低；
     * 改进方式 LazySingleton2 -》只需对其中的代码“instance = new LazySingleton();
     * @return
     */
    synchronized public static LazySingleton getInstance() {
        if (instance == null) {
            instance = new LazySingleton();
        }
        return instance;
    }
}
