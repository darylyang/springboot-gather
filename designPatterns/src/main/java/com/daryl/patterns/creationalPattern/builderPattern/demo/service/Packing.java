package com.daryl.patterns.creationalPattern.builderPattern.demo.service;

/**
 * @author by ly
 * @create 2018/9/18 9:54
 */
public interface Packing {
    String pack();
}
