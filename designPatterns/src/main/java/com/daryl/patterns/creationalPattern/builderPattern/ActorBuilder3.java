package com.daryl.patterns.creationalPattern.builderPattern;

/**
 * @  省略Director
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2019/8/6 23:26
 */
public abstract class ActorBuilder3 {

    protected Actor actor = new Actor();

    public abstract void buildType();

    public abstract void buildSex();

    public abstract void buildFace();

    public abstract void buildCostume();

    public abstract void buildHairstyle();

    public Actor construct() {
        this.buildType();
        this.buildSex();
        this.buildFace();
        this.buildCostume();
        this.buildHairstyle();
        return actor;
    }
}
