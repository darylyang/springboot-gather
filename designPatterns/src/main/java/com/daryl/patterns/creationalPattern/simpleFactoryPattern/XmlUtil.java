package com.daryl.patterns.creationalPattern.simpleFactoryPattern;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

/**
 * @
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2019/7/30 21:42
 */
public class XmlUtil {
    /**
     * 该方法用于从XML配置文件中提取图表类型，并返回类型名
     *
     * @return
     */
    public static String getChartType() {
        try {
            //创建文档对象
            DocumentBuilderFactory dFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = dFactory.newDocumentBuilder();
            Document doc;
            doc = builder.parse(new File("E:\\github\\design-pattern-ly\\src\\main\\java\\org\\daryl\\designPatterns" +
                    "\\creationalPattern\\simpleFactoryPattern\\config.xml"));

            //获取包含图表类型的文本节点
            NodeList nl = doc.getElementsByTagName("chartType");
            Node classNode = nl.item(0).getFirstChild();
            String chartType = classNode.getNodeValue().trim();
            return chartType;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}

class Client2 {
    public static void main(String args[]) {
        Chart chart;
        //读取配置文件中的参数
        String type = XmlUtil.getChartType();
        //创建产品对象
        chart = ChartFactory.getChart(type);
        chart.display();
    }
}
