package com.daryl.patterns.creationalPattern.builderPattern.demo.service.impl.food.sub;

import com.daryl.patterns.creationalPattern.builderPattern.demo.service.impl.food.Burger;

/**
 * @author by ly
 * @create 2018/9/18 10:05
 */
public class ChickenBurger extends Burger {
    @Override
    public String name() {
        return "Chicken Burger";
    }

    @Override
    public float price() {
        return 50.0f;
    }
}
