package com.daryl.patterns.creationalPattern.abstractFactoryPattern.demo1.service;

/**
 * @author by ly
 * @create 2018/8/31 9:06
 */
public interface Color {
    void fill();
}
