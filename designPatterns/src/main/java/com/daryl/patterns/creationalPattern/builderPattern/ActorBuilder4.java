package com.daryl.patterns.creationalPattern.builderPattern;

/**
 * @ 钩子方法的引入
 * 钩子方法的返回类型通常为boolean类型，方法名一般为isXXX()，钩子方法定义在抽象建造者 类中。
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2019/8/6 23:26
 */
public abstract class ActorBuilder4 {

    protected Actor actor = new Actor();

    public abstract void buildType();

    public abstract void buildSex();

    public abstract void buildFace();

    public abstract void buildCostume();

    public abstract void buildHairstyle();

    /**
     * 钩子方法
     * @return
     */
    public boolean isBareheaded() {
        return false;
    }

    public Actor createActor() {
        return actor;
    }
}
