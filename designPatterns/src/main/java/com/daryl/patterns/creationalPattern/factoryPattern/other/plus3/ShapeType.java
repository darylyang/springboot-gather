package com.daryl.patterns.creationalPattern.factoryPattern.other.plus3;

/**
 * @author by ly
 * @create 2018/8/28 19:50
 */
public enum ShapeType {
    CIRCLE,
    RECTANGLE,
    SQUARE
}
