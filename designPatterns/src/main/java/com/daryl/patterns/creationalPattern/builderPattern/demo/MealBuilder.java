package com.daryl.patterns.creationalPattern.builderPattern.demo;

import com.daryl.patterns.creationalPattern.builderPattern.demo.service.impl.food.sub.ChickenBurger;
import com.daryl.patterns.creationalPattern.builderPattern.demo.service.impl.food.sub.Coke;
import com.daryl.patterns.creationalPattern.builderPattern.demo.service.impl.food.sub.Pepsi;
import com.daryl.patterns.creationalPattern.builderPattern.demo.service.impl.food.sub.VegBurger;

/**
 * @author by ly
 * @create 2018/9/18 10:18
 */
public class MealBuilder {

    public Meal prepareVegMeal(){
        Meal meal = new Meal();
        meal.addItem(new VegBurger());
        meal.addItem(new Coke());
        return meal;
    }

    public Meal prepareNonVegMeal (){
        Meal meal = new Meal();
        meal.addItem(new ChickenBurger());
        meal.addItem(new Pepsi());
        return meal;
    }
}
