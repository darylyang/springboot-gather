package com.daryl.patterns.creationalPattern.abstractFactoryPattern.demo1;

import com.daryl.patterns.creationalPattern.abstractFactoryPattern.demo1.service.Color;
import com.daryl.patterns.creationalPattern.abstractFactoryPattern.demo1.service.Shape;
import com.daryl.patterns.creationalPattern.abstractFactoryPattern.demo1.service.impl.Blue;
import com.daryl.patterns.creationalPattern.abstractFactoryPattern.demo1.service.impl.Green;
import com.daryl.patterns.creationalPattern.abstractFactoryPattern.demo1.service.impl.Red;

/**
 * @author by ly
 * @create 2018/8/31 9:43
 */
public class ColorFactory extends AbstractFactory {
    @Override
    public Color getColor(String color) {
        if(color == null){
            return null;
        }
        if(color.equalsIgnoreCase("RED")){
            return new Red();
        } else if(color.equalsIgnoreCase("GREEN")){
            return new Green();
        } else if(color.equalsIgnoreCase("BLUE")){
            return new Blue();
        }
        return null;
    }

    @Override
    public Shape getShape(String shape) {
        return null;
    }
}
