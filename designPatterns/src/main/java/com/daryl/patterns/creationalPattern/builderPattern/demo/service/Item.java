package com.daryl.patterns.creationalPattern.builderPattern.demo.service;

/**
 * @author by ly
 * @create 2018/9/18 9:53
 */
public interface Item {
    String name();
    Packing packing();
    float price();
}
