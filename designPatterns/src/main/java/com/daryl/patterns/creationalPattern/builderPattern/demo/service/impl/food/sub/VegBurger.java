package com.daryl.patterns.creationalPattern.builderPattern.demo.service.impl.food.sub;

import com.daryl.patterns.creationalPattern.builderPattern.demo.service.impl.food.Burger;

/**
 * @author by ly
 * @create 2018/9/18 10:04
 */
public class VegBurger extends Burger {
    @Override
    public String name() {
        return "Veg Burger";
    }

    @Override
    public float price() {
        return 25.0f;
    }
}
