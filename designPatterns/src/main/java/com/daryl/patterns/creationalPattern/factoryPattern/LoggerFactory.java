package com.daryl.patterns.creationalPattern.factoryPattern;

import org.slf4j.Logger;

/**
 * @
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2019/8/1 7:32
 */
public interface LoggerFactory {
    Logger createLogger();

    Logger createLogger(String args);

    Logger createLogger(Object obj);

}
