package com.daryl.patterns.creationalPattern.factoryPattern.other.plus3;

import com.daryl.patterns.creationalPattern.factoryPattern.other.example.Circle;
import com.daryl.patterns.creationalPattern.factoryPattern.other.example.Rectangle;
import com.daryl.patterns.creationalPattern.factoryPattern.other.example.Shape;
import com.daryl.patterns.creationalPattern.factoryPattern.other.example.Square;

/**
 * @author by ly
 * @create 2018/8/28 19:50
 */
public class ShapeFactory4 {
    public static Shape getShape(ShapeType type) {
        switch (type) {
            case CIRCLE:
                return new Circle();
            case SQUARE:
                return new Square();
            case RECTANGLE:
                return new Rectangle();
            default:
                return null;
        }
    }
}
