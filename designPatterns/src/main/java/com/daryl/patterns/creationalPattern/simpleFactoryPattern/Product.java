package com.daryl.patterns.creationalPattern.simpleFactoryPattern;

/**
 * @
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2019/7/30 7:47
 */
public abstract class Product {
    /**
     * 所有产品类的公共业务方法
     */
    public void methodSame() {
        //公共方法的实现
    }

    /**
     * 声明抽象业务方法
     */
    public abstract void methodDiff();
}

class ConcreteProductA extends Product {
    /**
     * 实现业务方法
     */
    @Override
    public void methodDiff() {
        //业务方法的实现
        System.out.println("ConcreteProductA->methodDiff");
    }


}

class ConcreteProductB extends Product {
    /**
     * 实现业务方法
     */
    @Override
    public void methodDiff() {
        //业务方法的实现
        System.out.println("ConcreteProductB->methodDiff");
    }
}

class Factory {
    //静态工厂方法
    public static Product getProduct(String arg) {
        Product product = null;
        if (arg.equalsIgnoreCase("A")) {
            product = new ConcreteProductA();
            //初始化设置product
        }else if (arg.equalsIgnoreCase("B")) {
            product = new ConcreteProductB();
            //初始化设置product
        }
        return product;
    }
}

class Client{
    public static void main(String[] args) {
        Product product;
        product = Factory.getProduct("A");
        product.methodSame();
        product.methodDiff();
    }
}
