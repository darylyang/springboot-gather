package com.daryl.patterns.creationalPattern.builderPattern;

/**
 * @ 角色建造器：抽象建造者  省略Director
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2019/8/6 22:58
 */
public abstract class ActorBuilder2 {
    protected static Actor actor = new Actor();

    public abstract void buildType();

    public abstract void buildSex();

    public abstract void buildFace();

    public abstract void buildCostume();

    public abstract void buildHairstyle();

    public static Actor construct(ActorBuilder2 ab) {
        ab.buildType();
        ab.buildSex();
        ab.buildFace();
        ab.buildCostume();
        ab.buildHairstyle();
        return actor;
    }

}

//class Client2 {
//    public static void main(String args[]) {
//        ActorBuilder2 ab;
//        ab = new AngelBuilder();
//        Actor actor;
//        actor = ActorBuilder2.construct(ab);
//        String type = actor.getType();
//        System.out.println(type + "的外观：");
//        System.out.println("性别：" + actor.getSex());
//        System.out.println("面容：" + actor.getFace());
//        System.out.println("服装：" + actor.getCostume());
//        System.out.println("发型：" + actor.getHairstyle());
//    }
//}