package com.daryl.patterns.creationalPattern.builderPattern.demo.service.impl.food;

import com.daryl.patterns.creationalPattern.builderPattern.demo.service.Item;
import com.daryl.patterns.creationalPattern.builderPattern.demo.service.Packing;
import com.daryl.patterns.creationalPattern.builderPattern.demo.service.impl.pack.Wrapper;

/**
 * @author by ly
 * @create 2018/9/18 9:59
 */
public abstract class Burger implements Item {

    @Override
    public Packing packing() {
        return new Wrapper();
    }

    @Override
    public abstract float price();
}
