package com.daryl.patterns.creationalPattern.prototypePattern;

/**
 * @
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2019/8/6 7:25
 */
//public class ConcretePrototype implements Prototype{
//    /**
//     * 成员属性
//     */
//    private String attr;
//
//    public String getAttr() {
//        return attr;
//    }
//
//    public void setAttr(String attr) {
//        this.attr = attr;
//    }
//
//    /**
//     * 克隆方法
//     * @return
//     */
//    public Prototype clone() {
//        Prototype prototype = new ConcretePrototype();
//        ((ConcretePrototype) prototype).setAttr(this.attr);
//        return prototype;
//    }
//}
