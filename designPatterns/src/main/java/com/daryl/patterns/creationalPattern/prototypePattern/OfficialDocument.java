package com.daryl.patterns.creationalPattern.prototypePattern;

import java.util.Hashtable;

/**
 * @
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2019/8/6 21:32
 */
public interface OfficialDocument extends Cloneable {
    OfficialDocument clone();

    void display();
}

/**
 * 可行性分析报告(Feasibility Analysis Report)类
 */
class FAR implements OfficialDocument {

    @Override
    public OfficialDocument clone() {
        OfficialDocument far = null;
        try {
            far = (OfficialDocument) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            System.out.println("不支持复制！");
        }
        return far;
    }

    @Override
    public void display() {
        System.out.println("《可行性分析报告》");
    }
}

/**
 * 软件需求规格说明书(Software Requirements Specification)类
 */
class SRS implements OfficialDocument {
    @Override
    public OfficialDocument clone() {
        OfficialDocument srs = null;
        try {
            srs = (OfficialDocument) super.clone();
        } catch (CloneNotSupportedException e) {
            System.out.println("不支持复制！");
        }
        return srs;
    }

    @Override
    public void display() {
        System.out.println("《软件需求规格说明书》");
    }
}

/**
 * 原型管理器（使用饿汉式单例实现）
 * PrototypeManager类 提供了类似工厂方法的getOfficialDocument()方法用于返回一个克隆对象。
 */
class PrototypeManager{

    //定义一个Hashtable，用于存储原型对象
    private Hashtable ht = new Hashtable();
    private static PrototypeManager pm = new PrototypeManager();

    public PrototypeManager() {
       ht.put("far", new FAR());
       ht.put("srs", new SRS());
    }

    /**
     * 增加新的公文对象
     * @param key
     * @param doc
     */
    public void addOfficialDocument(String key, OfficialDocument doc) {
        ht.put(key, doc);
    }

    /**
     * 通过浅克隆 获取新的公文对象
     * @param key
     * @return
     */
    public OfficialDocument getOfficialDocument(String key) {
        return ((OfficialDocument) ht.get(key)).clone();
    }

    public static PrototypeManager getPrototypeManager() {
        return pm;
    }
}

/**
 * 《可行性分析报告》
 * 《可行性分析报告》
 * false
 * 《软件需求规格说明书》
 * 《软件需求规格说明书》
 * false
 */
class Client4 {
    public static void main(String[] args) {
        //获取原型管理器对象
        PrototypeManager pm = new PrototypeManager();
        OfficialDocument doc1, doc2, doc3, doc4;

        doc1 = pm.getOfficialDocument("far");
        doc1.display();
        doc2 = pm.getOfficialDocument("far");
        doc2.display();
        System.out.println(doc1 == doc2);

        doc3 = pm.getOfficialDocument("srs");
        doc3.display();
        doc4 = pm.getOfficialDocument("srs");
        doc4.display();
        System.out.println(doc3 == doc4);

    }
}