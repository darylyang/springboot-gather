package com.daryl.patterns.creationalPattern.builderPattern.demo.service.impl.food.sub;

import com.daryl.patterns.creationalPattern.builderPattern.demo.service.impl.food.ColdDrink;

/**
 * @author by ly
 * @create 2018/9/18 10:11
 */
public class Pepsi extends ColdDrink {
    @Override
    public String name() {
        return "Pepsi";
    }

    @Override
    public float price() {
        return 35.0f;
    }
}
