package com.daryl.patterns.creationalPattern.singletonPattern;

/**
 * @
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2019/8/1 23:39
 */
public class LazySingleton2 {

    private static LazySingleton2 instance = null;

    private LazySingleton2() {
    }

    /**
     * 只需对其中的代码“instance = new LazySingleton();
     * @return
     */
    public static LazySingleton2 getInstance() {
        if (instance == null) {
            synchronized (LazySingleton2.class) {
                instance = new LazySingleton2();
            }
        }
        return instance;
    }
}
