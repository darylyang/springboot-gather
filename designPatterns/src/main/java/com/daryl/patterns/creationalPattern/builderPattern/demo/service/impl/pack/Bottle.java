package com.daryl.patterns.creationalPattern.builderPattern.demo.service.impl.pack;

import com.daryl.patterns.creationalPattern.builderPattern.demo.service.Packing;

/**
 * @author by ly
 * @create 2018/9/18 9:58
 */
public class Bottle implements Packing {
    @Override
    public String pack() {
        return "Bottle";
    }
}
