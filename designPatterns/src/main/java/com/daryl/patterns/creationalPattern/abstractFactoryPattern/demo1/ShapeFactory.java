package com.daryl.patterns.creationalPattern.abstractFactoryPattern.demo1;

import com.daryl.patterns.creationalPattern.abstractFactoryPattern.demo1.service.Color;
import com.daryl.patterns.creationalPattern.abstractFactoryPattern.demo1.service.Shape;
import com.daryl.patterns.creationalPattern.abstractFactoryPattern.demo1.service.impl.Circle;
import com.daryl.patterns.creationalPattern.abstractFactoryPattern.demo1.service.impl.Rectangle;
import com.daryl.patterns.creationalPattern.abstractFactoryPattern.demo1.service.impl.Square;

/**
 * @author by ly
 * @create 2018/8/31 9:24
 */
public class ShapeFactory extends AbstractFactory {
    @Override
    public Color getColor(String color) {
        return null;
    }

    @Override
    public Shape getShape(String shapeType) {
        if(shapeType == null){
            return null;
        }
        if(shapeType.equalsIgnoreCase("CIRCLE")){
            return new Circle();
        } else if(shapeType.equalsIgnoreCase("RECTANGLE")){
            return new Rectangle();
        } else if(shapeType.equalsIgnoreCase("SQUARE")){
            return new Square();
        }
        return null;
    }
}
