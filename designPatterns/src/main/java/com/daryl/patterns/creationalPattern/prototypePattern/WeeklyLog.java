package com.daryl.patterns.creationalPattern.prototypePattern;

/**
 * @
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2019/8/6 7:38
 */
public class WeeklyLog implements Cloneable {

    private String name;
    private String date;
    private String content;

    private Attachment attachment;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Attachment getAttachment() {
        return attachment;
    }

    public void setAttachment(Attachment attachment) {
        this.attachment = attachment;
    }

    @Override
    public WeeklyLog clone() {
        Object obj = null;
        try {
            obj = super.clone();
            return (WeeklyLog) obj;
        } catch (CloneNotSupportedException e) {
            System.out.println("不支持复制！" + e);
            return null;
        }
    }
}

class Attachment {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void download() {
        System.out.println("下载附件，文件名为" + name);
    }
}

/**
 * ****周报****
 * 周次：第12周
 * 姓名：张无忌
 * 内容：这周工作很忙，每天加班！
 * --------------------------------
 * ****周报****
 * 周次：第13周
 * 姓名：张无忌
 * 内容：这周工作很忙，每天加班！
 * --------------------------------
 * false
 * false
 * true
 * true
 */
class Client {
    public static void main(String[] args) {
        // 创建原型对象
        WeeklyLog log_previous = new WeeklyLog();
        log_previous.setName("张无忌");
        log_previous.setDate("第12周");
        log_previous.setContent("这周工作很忙，每天加班！");

        System.out.println("****周报****");
        System.out.println("周次：" + log_previous.getDate());
        System.out.println("姓名：" + log_previous.getName());
        System.out.println("内容：" + log_previous.getContent());
        System.out.println("--------------------------------");

        WeeklyLog log_new;
        //调用克隆方法创建克隆对象
        log_new = log_previous.clone();
        log_new.setDate("第13周");
        System.out.println("****周报****");
        System.out.println("周次：" + log_new.getDate());
        System.out.println("姓名：" + log_new.getName());
        System.out.println("内容：" + log_new.getContent());

        System.out.println("--------------------------------");
        System.out.println(log_previous == log_new);
        System.out.println(log_previous.getDate() == log_new.getDate());
        System.out.println(log_previous.getName() == log_new.getName());
        System.out.println(log_previous.getContent() == log_new.getContent());
    }
}

/**
 * 周报是否相同？ false
 * 附件是否相同？ true
 */
class Client2 {
    public static void main(String[] args) {
        WeeklyLog log_previous, log_new;
        //创建原型对象
        log_previous = new WeeklyLog();
        //创建附件对象
        Attachment attachment = new Attachment();
        //将附件添加到周报中
        log_previous.setAttachment(attachment);
        //调用克隆方法创建克隆对象
        log_new = log_previous.clone();
        //比较周报
        System.out.println("周报是否相同？ " + (log_previous == log_new));
        //比较附件
        System.out.println("附件是否相同？ " + (log_previous.getAttachment() == log_new.getAttachment()));
    }
}