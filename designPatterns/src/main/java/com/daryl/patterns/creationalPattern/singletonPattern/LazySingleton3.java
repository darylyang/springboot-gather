package com.daryl.patterns.creationalPattern.singletonPattern;

/**
 * @ 如果使用双重检查锁定来实现懒汉式单例类，需要在静态成员变量instance之 前增加修饰符volatile，被volatile修饰的成员变量可以确保多个线程都能够正确处理，且该代 码只能在JDK 1.5及以上版本中才能正确执行。由于volatile关键字会屏蔽Java虚拟机所做的一 些代码优化，可能会导致系统运行效率降低，因此即使使用双重检查锁定来实现单例模式也 不是一种完美的实现方式。
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2019/8/1 23:39
 */
public class LazySingleton3 {

    private volatile static LazySingleton3 instance = null;

    private LazySingleton3() {
    }

    /**
     * @return
     */
    public static LazySingleton3 getInstance() {
        if (instance == null) {
            synchronized (LazySingleton3.class) {
                if (instance == null) {
                    instance = new LazySingleton3();
                }
            }
        }
        return instance;
    }
}
