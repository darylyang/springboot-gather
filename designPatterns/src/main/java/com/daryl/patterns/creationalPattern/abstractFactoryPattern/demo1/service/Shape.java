package com.daryl.patterns.creationalPattern.abstractFactoryPattern.demo1.service;

/**
 * @author by ly
 * @create 2018/8/28 18:25
 */
public interface Shape {
    void draw();
}
