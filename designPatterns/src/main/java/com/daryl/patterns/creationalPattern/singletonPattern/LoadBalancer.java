package com.daryl.patterns.creationalPattern.singletonPattern;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @ 将负载均衡器LoadBalancer设计为单例类，其中包含一个存储服务器信息的集合 serverList，每次在serverList中随机选择一台服务器来响应客户端的请求
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2019/8/1 23:10
 */
public class LoadBalancer {
    /**
     * 私有静态成员变量，存储唯一实例
     */
    private static LoadBalancer instance = null;
    /**
     * 服务器集合
     */
    private List serverList = null;

    /**
     * 私有构造函数
     */
    private LoadBalancer() {
        serverList = new ArrayList();
    }

    /**
     * 公有静态成员方法，返回唯一实例
     *
     * @return
     */
    public static LoadBalancer getLoadBalancer() {
        if (instance == null) {
            instance = new LoadBalancer();
        }
        return instance;
    }

    /**
     * 增加服务器
     */
    public void addServer(String server) {
        serverList.add(server);
    }

    /**
     * 删除服务器
     */
    public void removeServer(String server) {
        serverList.remove(server);
    }

    /**
     * 使用Random类随机获取服务器
     *
     * @return
     */
    public String getServer() {
        Random random = new Random();
        int i = random.nextInt(serverList.size());
        return (String) serverList.get(i);
    }

}

/**
 * 虽然创建了四个LoadBalancer对象，但是它们实际上是同一个对象，因此，通过使用单例模式 可以确保LoadBalancer对象的唯一性。
 */
class Client {
    public static void main(String args[]) {
        //创建四个LoadBalancer对象
        LoadBalancer balancer1, balancer2, balancer3, balancer4;
        balancer1 = LoadBalancer.getLoadBalancer();
        balancer2 = LoadBalancer.getLoadBalancer();
        balancer3 = LoadBalancer.getLoadBalancer();
        balancer4 = LoadBalancer.getLoadBalancer();

        //判断服务器负载均衡器是否相同
        if (balancer1 == balancer2 && balancer2 == balancer3 && balancer3 == balancer4) {
            System.out.println("服务器负载均衡器具有唯一性！");
        }

        //增加服务器
        balancer1.addServer("Server 1");
        balancer1.addServer("Server 2");
        balancer1.addServer("Server 3");
        balancer1.addServer("Server 4");

        //模拟客户端请求的分发
        for (int i = 0; i < 10; i++) {
            String server = balancer1.getServer();
            System.out.println("分发请求至服务器： " + server);
        }
    }
}