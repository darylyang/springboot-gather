package com.daryl.patterns.creationalPattern.factoryPattern.old;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

/**
 * @
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2019/7/30 21:42
 */
public class XmlUtil {
    /**
     * 该方法用于从XML配置文件中提取具体类名，并返回一个实例对象
     *
     * @return
     */
    public static Object getBean() {
        try {
            //创建文档对象
            DocumentBuilderFactory dFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = dFactory.newDocumentBuilder();
            Document doc;
            doc = builder.parse(new File("E:\\github\\design-pattern-ly\\src\\main\\java\\org\\daryl\\designPatterns" +
                    "\\creationalPattern\\factoryPattern\\config.xml"));

            //获取包含类名的文本节点
            NodeList nl = doc.getElementsByTagName("className");
            Node classNode = nl.item(0).getFirstChild();
            String cName = classNode.getNodeValue().trim();

            // 通过类名生成实例对象并将其返回
            Class c = Class.forName(cName);
            Object obj = c.newInstance();
            return obj;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}

class Client2 {
    public static void main(String args[]) {
//        LoggerFactory factory;
//        Logger logger;
//        factory = (LoggerFactory) XmlUtil.getBean();
////        factory = new FileLoggerFactory();
//        logger = factory.createLogger();
//        logger.writeLog();

//        Class c = null;
//        try {
//            c = Class.forName("String");
//            Object obj = c.newInstance();
//            System.out.println(obj);
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//        } catch (InstantiationException e) {
//            e.printStackTrace();
//        }
    }
}