package com.daryl.patterns.creationalPattern.factoryPattern;

import org.slf4j.Logger;

/**
 * @
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2019/8/1 7:40
 */
public abstract class LoggerFactory2 {

    public abstract Logger createLogger();

    public void writeLog() {
        Logger logger = this.createLogger();
    }
}


