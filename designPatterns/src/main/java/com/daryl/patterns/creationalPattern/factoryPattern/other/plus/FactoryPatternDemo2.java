package com.daryl.patterns.creationalPattern.factoryPattern.other.plus;

import com.daryl.patterns.creationalPattern.factoryPattern.other.example.Circle;
import com.daryl.patterns.creationalPattern.factoryPattern.other.example.Rectangle;
import com.daryl.patterns.creationalPattern.factoryPattern.other.example.Square;

/**
 * @author by ly
 * @create 2018/8/28 19:23
 */
public class FactoryPatternDemo2 {
    public static void main(String[] args) {
        ShapeFactory2 shapeFactory2 = new ShapeFactory2();

        Rectangle rect = (Rectangle) shapeFactory2.getClass(Rectangle.class);
        rect.draw();

        Square square = (Square) shapeFactory2.getClass(Square.class);
        square.draw();

        Circle circle = (Circle) shapeFactory2.getClass(Circle.class);
        circle.draw();
    }

}
