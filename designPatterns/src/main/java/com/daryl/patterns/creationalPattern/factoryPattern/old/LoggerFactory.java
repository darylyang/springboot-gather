package com.daryl.patterns.creationalPattern.factoryPattern.old;

/**
 * 日志记录器工厂接口：抽象工厂
 *
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2019/7/31 22:20
 */
interface LoggerFactory {
    Logger createLogger();
}

/**
 * 日志记录器接口：抽象产品
 */
interface Logger {
    void writeLog();
}

/**
 * 数据库日志记录器：具体产品
 */
class DatabaseLogger implements Logger {
    @Override
    public void writeLog() {
        System.out.println("数据库日志记录。");
    }
}

/**
 * 文件日志记录器：具体产品
 */
class FileLogger implements Logger {
    @Override
    public void writeLog() {
        System.out.println("文件日志记录。");
    }
}

/**
 * 数据库日志记录器工厂类：具体工厂
  */
class DatabaseLoggerFactory implements LoggerFactory {
    @Override
    public Logger createLogger() {
        //初始化数据库日志记录器，代码省略
        Logger logger = new DatabaseLogger();
        return logger;
    }
}

/**
 * 文件日志记录器工厂类：具体工厂
 */
class FileLoggerFactory implements LoggerFactory {
    @Override
    public Logger createLogger() {
        //创建文件日志记录器对象
        Logger logger = new FileLogger();
        return logger;
    }
}

class Client {
    public static void main(String args[]) {
        LoggerFactory factory;
        Logger logger;
        //可引入配置文件实现
        factory = new FileLoggerFactory();
        logger = factory.createLogger();
        logger.writeLog();
    }
}

