package com.daryl.patterns.creationalPattern.simpleFactoryPattern.old;

/**
 * 客户端代码通过调用Chart类的构造函数来创建图表对象，根据参数type的不同可以得到不同 类型的图表，然后再调用display()方法来显示相应的图表。
 *
 * (1) 在Chart类中包含很多“if…else…”代码块，整个类的代码相当冗长，代码越长，阅读难度、 维护难度和测试难度也越大；
 * 而且大量条件语句的存在还将影响系统的性能，程序在执行过 程中需要做大量的条件判断。
 * (2) Chart类的职责过重，负责初始化和显示所有的图表对象，将各种图表对象的初始化代码 和显示代码集中在一个类中实现，违反了“单一职责原则”，不利于类的重用和维护；
 * 而且将大 量的对象初始化代码都写在构造函数中将导致构造函数非常庞大，对象在创建时需要进行条 件判断，降低了对象创建的效率。
 * (3) 当需要增加新类型的图表时，必须修改Chart类的源代码，违反了“开闭原则”。
 * (4) 客户端只能通过new关键字来直接创建Chart对象，Chart类与客户端类耦合度较高，对象的 创建和使用无法分离。
 * (5) 客户端在创建Chart对象之前可能还需要进行大量初始化设置，代码的重复
 *
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2019/7/29 23:58
 */
public class Chart {

    /**
     * 图表类型
     */
    private String type;

    public Chart(Object[][] data, String type) {
        this.type = type;
        if (type.equalsIgnoreCase("histogram")) {
            //初始化柱状图
        }else if (type.equalsIgnoreCase("pie")) {
            //初始化饼状图
        }else if (type.equalsIgnoreCase("line")) {
            //初始化折线图
        }
    }

    public void display() {
        if (this.type.equalsIgnoreCase("histogram")) {
            //显示柱状图
        }else if (this.type.equalsIgnoreCase("pie")) {
            //显示饼状图
        }else if (this.type.equalsIgnoreCase("line")) {
            //显示折线图
        }
    }


}
