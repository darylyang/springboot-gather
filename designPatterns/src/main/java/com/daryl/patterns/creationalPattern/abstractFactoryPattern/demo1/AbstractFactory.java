package com.daryl.patterns.creationalPattern.abstractFactoryPattern.demo1;

import com.daryl.patterns.creationalPattern.abstractFactoryPattern.demo1.service.Color;
import com.daryl.patterns.creationalPattern.abstractFactoryPattern.demo1.service.Shape;

/**
 * @author by ly
 * @create 2018/8/31 9:12
 */
public abstract class AbstractFactory {
    public abstract Color getColor(String color);
    public abstract Shape getShape(String shape);
}
