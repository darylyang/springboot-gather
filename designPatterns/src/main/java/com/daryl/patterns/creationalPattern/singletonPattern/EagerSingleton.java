package com.daryl.patterns.creationalPattern.singletonPattern;

/**
 * @ 饿汉式单例
 * 在定义静态变量的时候实例化单例类，因此在类加载的时候就已经 创建了单例对象
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2019/8/1 23:37
 */
public class EagerSingleton {
    private static final EagerSingleton instance = new EagerSingleton();

    private EagerSingleton() {
    }

    public static EagerSingleton getInstance() {
        return instance;
    }
}
