package com.daryl.patterns.creationalPattern.prototypePattern;

import java.io.*;

/**
 * @
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2019/8/6 21:10
 */
public class WeeklyLog2 implements Serializable {

    private Attachment2 attachment2;
    private String name;
    private String date;
    private String content;

    public Attachment2 getAttachment2() {
        return attachment2;
    }

    public void setAttachment2(Attachment2 attachment2) {
        this.attachment2 = attachment2;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    /**
     * 使用序列化技术实现 深克隆
     *
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public WeeklyLog2 deepClone() throws IOException, ClassNotFoundException {
        // 将对象写入流中
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bos);
        oos.writeObject(this);

        // 将对象从流中取出
        ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
        ObjectInputStream ois = new ObjectInputStream(bis);
        return (WeeklyLog2) ois.readObject();
    }
}

class Attachment2 implements Serializable {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void download() {
        System.out.println("下载附件，文件名为" + name);
    }
}

/**
 * 使用了深克隆技术，附件对象也得以复制，因此用“==”比较原型 对象的附件和克隆对象的附件时输出结果均为false。
 * 深克隆技术实现了原型对象和克隆对象的 完全独立，对任意克隆对象的修改都不会给其他对象产生影响，是一种更为理想的克隆实现 方式。
 * 周报是否相同？ false
 * 附件是否相同？ false
 */
class Client3 {
    public static void main(String[] args) {
        WeeklyLog2 log_previous, log_new = null;
        log_previous = new WeeklyLog2();
        Attachment2 attachment2 = new Attachment2();
        log_previous.setAttachment2(attachment2);
        try {
            // 调用深克隆
            log_new = log_previous.deepClone();
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("克隆失败！！！");
            ;
        }
        System.out.println("周报是否相同？ " + (log_previous == log_new));
        System.out.println("附件是否相同？ " + (log_previous.getAttachment2() == log_new.getAttachment2()));
    }
}

