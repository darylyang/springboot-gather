package com.daryl.patterns.creationalPattern.builderPattern.demo.service.impl.food;

import com.daryl.patterns.creationalPattern.builderPattern.demo.service.Item;
import com.daryl.patterns.creationalPattern.builderPattern.demo.service.Packing;
import com.daryl.patterns.creationalPattern.builderPattern.demo.service.impl.pack.Bottle;

/**
 * @author by ly
 * @create 2018/9/18 10:03
 */
public abstract class ColdDrink implements Item {

    @Override
    public Packing packing() {
        return new Bottle();
    }

    @Override
    public abstract float price();
}
