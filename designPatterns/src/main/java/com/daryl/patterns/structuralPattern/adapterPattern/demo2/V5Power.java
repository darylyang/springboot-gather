package com.daryl.patterns.structuralPattern.adapterPattern.demo2;

/**
 * 提供5V电压的一个接口
 *
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/9/22 22:23
 */
public interface V5Power {
    int provideV5Power();
}
