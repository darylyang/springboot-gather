package com.daryl.patterns.structuralPattern.bridgePattern;

/**
 * @
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2019/8/9 7:33
 */
public interface ImageImp {
    /**
     * 显示像素矩阵
     * @param m
     */
    void doPaint(Matrix m);
}
