package com.daryl.patterns.structuralPattern.adapterPattern.demo2;

/**
 * 需要一个适配器，完成220V转5V的作用
 *
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/9/22 22:25
 */
public class V5PowerAdapter implements V5Power {
    /**
     * 组合的方式
     */
    private V220Power v220Power;

    public V5PowerAdapter(V220Power v220Power) {
        this.v220Power = v220Power;
    }

    @Override
    public int provideV5Power() {
        int power = v220Power.provideV220Power();
        //power经过各种操作-->5
        System.out.println("适配器：我悄悄的适配了电压。");
        return 5;
    }

    public static void main(String[] args) {
        Mobile mobile = new Mobile();
        V5Power v5Power = new V5PowerAdapter(new V220Power());
        mobile.inputPower(v5Power);
    }

}

