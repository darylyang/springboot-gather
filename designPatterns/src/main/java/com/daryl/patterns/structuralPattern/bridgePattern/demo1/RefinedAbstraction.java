package com.daryl.patterns.structuralPattern.bridgePattern.demo1;

/**
 * @
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2019/8/9 7:13
 */
public class RefinedAbstraction extends Abstraction {
    @Override
    public void operation() {
        impl.operationImpl();
    }
}
