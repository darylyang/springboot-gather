package com.daryl.patterns.structuralPattern.adapterPattern.demo1;

/**
 * @
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2019/8/8 22:32
 */
public class OperationAdapter implements ScoreOperation {

    private QuickSort sortObj;
    private BianrySearch searchObj;

    public OperationAdapter() {
        sortObj = new QuickSort();
        searchObj = new BianrySearch();
    }

    @Override
    public int[] sort(int[] array) {
        return sortObj.quickSort(array);
    }

    @Override
    public int search(int[] array, int key) {
        return searchObj.binarySearch(array, key);
    }
}
