package com.daryl.patterns.structuralPattern.adapterPattern.demo2;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/9/22 22:24
 */
public class V220Power {
    /**
     * 提供220V电压
     *
     * @return
     */
    public int provideV220Power() {
        System.out.println("我提供220V交流电压。");
        return 220;
    }
}
