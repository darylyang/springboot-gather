package com.daryl.patterns.structuralPattern.adapterPattern.demo1;

/**
 * @
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2019/8/6 23:47
 */
//public class Adapter extends Target {
//    private Adaptee adaptee; //维持一个对适配者对象的引用
//
//    public Adapter(Adaptee adaptee) {
//        this.adaptee = adaptee;
//    }
//
//    public void request() {
//        adaptee.specificRequest(); //转发调用 }
//    }
//}