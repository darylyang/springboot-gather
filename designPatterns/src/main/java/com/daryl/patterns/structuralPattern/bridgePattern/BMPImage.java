package com.daryl.patterns.structuralPattern.bridgePattern;

/**
 * @ BMP格式图像：扩充抽象类
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2019/8/10 13:24
 */
public class BMPImage extends Image {
    @Override
    public void parseFile(String fileName) {
        //模拟解析BMP文件并获得一个像素矩阵对象m;
        Matrix m = new Matrix();
        imp.doPaint(m);
        System.out.println(fileName + "，格式为BMP。");
    }
}
