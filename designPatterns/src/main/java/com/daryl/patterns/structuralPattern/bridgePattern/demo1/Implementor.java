package com.daryl.patterns.structuralPattern.bridgePattern.demo1;

/**
 * @
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2019/8/9 7:11
 */
public interface Implementor {
    void operationImpl();
}
