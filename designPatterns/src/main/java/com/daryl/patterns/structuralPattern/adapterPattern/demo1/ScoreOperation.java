package com.daryl.patterns.structuralPattern.adapterPattern.demo1;

/**
 * @ 抽象成绩操作类：目标接口
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2019/8/8 22:09
 */
public interface ScoreOperation {
    /**
     * 成绩排序
     *
     * @param array
     * @return
     */
    int[] sort(int[] array);

    /**
     * 成绩查找
     *
     * @param array
     * @param key
     * @return
     */
    int search(int[] array, int key);
}