package com.daryl.patterns.structuralPattern.bridgePattern;

import ch.qos.logback.core.joran.spi.XMLUtil;

/**
 * @
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2019/8/10 13:32
 */
public class BridgeClient {
    public static void main(String args[]) {
        Image image = new JPGImage();
        ImageImp imp = new WindowsImp();

        image.setImp(imp);
        image.parseFile("小龙女");
    }
}
