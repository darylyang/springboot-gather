package com.daryl.rabbitmq.rabbit;

import com.daryl.rabbitmq.rabbit.topic.TopicSender;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TopicTest {

    @Autowired
    private TopicSender sender;

    /**
     * Topic Receiver2  : hi, i am message all
     *
     * @throws Exception
     */
    @Test
    public void topic() throws Exception {
        sender.send();
    }

    /**
     * Topic Receiver1  : hi, i am message 1
     * Topic Receiver2  : hi, i am message 1
     *
     * @throws Exception
     */
    @Test
    public void topic1() throws Exception {
        sender.send1();
    }

    /**
     *
     * Topic Receiver2  : hi, i am messages 2
     *
     * @throws Exception
     */
    @Test
    public void topic2() throws Exception {
        sender.send2();
    }

}