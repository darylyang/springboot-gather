package com.daryl.rabbitmq.rabbit;

import com.daryl.rabbitmq.rabbit.many.DarylSender;
import com.daryl.rabbitmq.rabbit.many.DarylSender2;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ManyTest {
	@Autowired
	private DarylSender darylSender;

	@Autowired
	private DarylSender2 darylSender2;

	@Test
	public void oneToMany() throws Exception {
		for (int i=0;i<100;i++){
			darylSender.send(i);
		}
	}

	@Test
	public void manyToMany() throws Exception {
		for (int i=0;i<100;i++){
			darylSender.send(i);
			darylSender2.send(i);
		}
	}

}