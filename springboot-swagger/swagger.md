knife4j是为Java MVC框架集成Swagger生成Api文档的增强解决方案,前身是swagger-bootstrap-ui,取名kni4j是希望她能像一把匕首一样小巧,轻量,并且功能强悍!

knife4j的前身是`swagger-bootstrap-ui`，为了契合微服务的架构发展,由于原来`swagger-bootstrap-ui`采用的是后端Java代码+前端Ui混合打包的方式,在微服务架构下显的很臃肿,因此项目正式更名为`knife4j`

更名后主要专注的方面

- 前后端Java代码以及前端Ui模块进行分离,在微服务架构下使用更加灵活
- 提供专注于Swagger的增强解决方案,不同于只是改善增强前端Ui部分


**效果(2.0版)：**[http://knife4j.xiaominfo.com/doc.html](http://knife4j.xiaominfo.com/doc.html)

**效果(旧版本)：**[http://swagger-bootstrap-ui.xiaominfo.com/doc.html](http://swagger-bootstrap-ui.xiaominfo.com/doc.html)

**示例:**[https://gitee.com/xiaoym/swagger-bootstrap-ui-demo](https://gitee.com/xiaoym/swagger-bootstrap-ui-demo)


**文档**:[https://doc.xiaominfo.com/](https://doc.xiaominfo.com/)

**源码分析**:[https://www.xiaominfo.com/2019/05/20/springfox-0/](https://www.xiaominfo.com/2019/05/20/springfox-0/)

## 项目模块

目前主要的模块包括：

| 模块名称             | 说明                                                         |
| -------------------- | ------------------------------------------------------------ |
| knife4j              | 为Java MVC框架集成Swagger的增强解决方案                      |
| knife4j-admin        | 云端Swagger接口文档注册管理中心,集成gateway网关对任意微服务文档进行组合集成 |
| knife4j-extension    | chrome浏览器的增强swagger接口文档ui,快速渲染swagger资源      |
| knife4j-service      | 为swagger服务的一系列接口服务程序                            |
| knife4j-front        | knife4j-spring-ui的纯前端静态版本,用于集成非Java语言使用     |
| swagger-bootstrap-ui | knife4j的前身,最后发布版本是1.9.6                            |



## 业务场景

### 不使用增强功能,纯粹换一个swagger的前端皮肤

不使用增强功能,纯粹换一个swagger的前端皮肤，这种情况是最简单的,你项目结构下无需变更

可以直接引用swagger-bootstrap-ui的最后一个版本1.9.6或者使用knife4j-spring-ui

老版本引用

```xml
<dependency>
    <groupId>com.github.xiaoymin</groupId>
    <artifactId>swagger-bootstrap-ui</artifactId>
    <version>1.9.6</version>
</dependency>
```

新版本引用

```xml
<dependency>
    <groupId>com.github.xiaoymin</groupId>
    <artifactId>knife4j-spring-ui</artifactId>
    <version>${lastVersion}</version>
</dependency>
```


### 默认开启增强模式 
https://doc.xiaominfo.com/knife4j/autoEnableKnife4j.html

>knife4j 版本>2.0.1 使用此规则

1、Java后端必须在@EnableSwagger2的基础上再添加@EnableKnife4j增强注解
```java
@Configuration
@EnableSwagger2
@EnableKnife4j
public class SwaggerConfiguration {
    
 //more..

}
```

2、如果你使用Spring Security、Shiro这类权限框架时,需要对接口地址/v2/api-docs-ext进行放权

http://host:port/doc.html#/plus


@EnableSwagger2	该注解是Springfox-swagger框架提供的使用Swagger注解，该注解必须加
@EnableKnife4j	该注解是knife4j提供的增强注解,Ui提供了例如动态参数、参数过滤、接口排序等增强功能,如果你想使用这些增强功能就必须加该注解，否则可以不用加





### Spring Boot项目单体架构使用增强功能

在Spring Boot单体架构下,knife4j提供了starter供开发者快速使用

```xml
<dependency>
    <groupId>com.github.xiaoymin</groupId>
    <artifactId>knife4j-spring-boot-starter</artifactId>
    <version>${knife4j.version}</version>
</dependency>
```

该包会引用所有的knife4j提供的资源，包括前端Ui的jar包

### Spring Cloud微服务架构

在Spring Cloud的微服务架构下,每个微服务其实并不需要引入前端的Ui资源,因此在每个微服务的Spring Boot项目下,引入knife4j提供的微服务starter

```xml
<dependency>
    <groupId>com.github.xiaoymin</groupId>
    <artifactId>knife4j-micro-spring-boot-starter</artifactId>
    <version>${knife4j.version}</version>
</dependency>
```

在网关聚合文档服务下,可以再把前端的ui资源引入

```xml
<dependency>
    <groupId>com.github.xiaoymin</groupId>
    <artifactId>knife4j-spring-boot-starter</artifactId>
    <version>${knife4j.version}</version>
</dependency>
```

## 另外说明

不管是knife4j还是swagger-bootstrap-ui

对外提供的地址依然是doc.html

访问：http://ip:port/doc.html

即可查看文档

**这是永远不会改变的**




## 项目Demo示例

Demo示例见另外项目地址：[https://gitee.com/xiaoym/swagger-bootstrap-ui-demo](https://gitee.com/xiaoym/swagger-bootstrap-ui-demo)

| 模块                            | 说明                                                         |
| ------------------------------- | ------------------------------------------------------------ |
| knife4j-spring-boot-demo        | 在Spring Boot架构下集成knife4j的项目示例                     |
| knife4j-spring-boot-single-demo | 在Spring Boot单体架构下集成knife4j的项目示例                 |
| knife4j-spring-cloud-gateway    | 在Spring Cloud微服务架构下通过gateway网集成knife4j的示例     |
| swagger-bootstrap-ui-demo-mvc   | 在Spring MVC模式下集成swagger-bootstrap-ui                   |
| swagger-bootstrap-ui-demo       | 在Spring Boot单体架构下集成swagger-bootstrap-ui              |
| swagger-bootstrap-ui-gateway    | 在Spring Cloud微服务架构下通过gateway网关集成swagger-bootstrap-ui |
| swagger-bootstrap-ui-zuul       | 在Spring Cloud微服务架构下通过zuul网关集成swagger-bootstrap-ui |
