
## redis 配置

```
spring:
  #redis相关配置
  redis:
    database: 0
    # 配置redis的主机地址，需要修改成自己的
    host: localhost
    port: 6379
    #    password: 123456
    timeout: 5000
    #    lettuce:
    #      pool:
    #        # 连接池中的最大空闲连接，默认值也是8。
    #        max-idle: 8
    #        # 连接池中的最小空闲连接，默认值也是0。
    #        min-idle: 0
    #        # 连接池最大连接数（使用负值表示没有限制） 默认 8
    #        max-active: 8
    #        # 连接池最大阻塞等待时间（使用负值表示没有限制） 默认 -1
    #        max-wait: -1

    jedis:
      pool:
        # 连接池中的最大空闲连接，默认值也是8。
        max-idle: 500
        # 连接池中的最小空闲连接，默认值也是0。
        min-idle: 50
        # 如果赋值为-1，则表示不限制；如果pool已经分配了maxActive个jedis实例，则此时pool的状态为exhausted(耗尽)
        max-active: 1000
        # 等待可用连接的最大时间，单位毫秒，默认值为-1，表示永不超时。如果超过等待时间，则直接抛出JedisConnectionException
        max-wait: 2000
```


## redis api

有两个 redis 模板：RedisTemplate 和 StringRedisTemplate。
我们不使用 RedisTemplate，RedisTemplate 提供给我们操作对象，操作对象的时候，我们通常是以 json 格式存储，但在存储的时候，会使用 Redis 默认的内部序列化器；导致我们存进里面的是乱码之类的东西。
当然了，我们可以自己定义序列化，但是比较麻烦，所以使用 StringRedisTemplat模板。

StringRedisTemplate 主要给我们提供字符串操作，我们可以将实体类等转成 json 字符串即可，在取出来后，也可以转成相应的对象，这就是上面我导入了阿里 fastjson 的原因。


### redis:string 类型
新建一个 RedisService，注入 StringRedisTemplate，使用 stringRedisTemplate.opsForValue()可以获取 ValueOperations<String, String> 对象，通过该对象即可读写 redis 数据库了。如下：

```java
    @Resource
    private StringRedisTemplate stringRedisTemplate;


    /**
     * set redis: string类型
     * @param key key
     * @param value value
     */
    public void setString(String key, String value){
        ValueOperations<String, String> valueOperations = stringRedisTemplate.opsForValue();
        valueOperations.set(key, value);
    }

    /**
     * get redis: string类型
     * @param key key
     * @return
     */
    public String getString(String key){
        return stringRedisTemplate.opsForValue().get(key);
    }
``` 

### redis:hash 类型
hash 类型其实原理和 string 一样的，但是有两个 key，使用 stringRedisTemplate.opsForHash()
可以获取 HashOperations<String, Object, Object> 对象。
比如我们要存储订单信息，所有订单信息都放在 order 下，针对不同用户的订单实体，可以通过用户的 id 来区分，这就相当于两个 key了。

```java
    /**
     * set redis: hash类型
     * @param key key
     * @param filedKey filedkey
     * @param value value
     */
    public void setHash(String key, String filedKey, String value){
        HashOperations<String, Object, Object> hashOperations = stringRedisTemplate.opsForHash();
        hashOperations.put(key, filedKey, value);
    }

    /**
     * get redis: hash类型
     * @param key key
     * @param filedkey filedkey
     * @return
     */
    public String getHash(String key, String filedkey){
        return (String) stringRedisTemplate.opsForHash().get(key, filedkey);
    }
```


### redis:list 类型
使用 stringRedisTemplate.opsForList() 可以获取 ListOperations<String, String> listOperations redis 列表对象，该列表是个简单的字符串列表，可以支持从左侧添加，也可以支持从右侧添加，一个列表最多包含 2 ^ 32 -1 个元素。

```java
    /**
     * set redis:list类型
     * @param key key
     * @param value value
     * @return
     */
    public long setList(String key, String value){
        ListOperations<String, String> listOperations = stringRedisTemplate.opsForList();
        return listOperations.leftPush(key, value);
    }

    /**
     * get redis:list类型
     * @param key key
     * @param start start
     * @param end end
     * @return
     */
    public List<String> getList(String key, long start, long end){
        return stringRedisTemplate.opsForList().range(key, start, end);
    }
```


