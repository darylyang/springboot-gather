package com.daryl.redis;

import com.alibaba.fastjson.JSON;
import com.daryl.redis.model.User;
import com.daryl.redis.service.RedisService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@SpringBootTest
class RedisApplicationTests {

	@Resource
	private RedisService redisService;

	@Test
	void contextLoads() {
		//测试redis的string类型
		redisService.setString("weichat","wqerqwerqwe");
		log.info("qreqwerqwer为：{}", redisService.getString("weichat"));

		// 如果是个实体，我们可以使用json工具转成json字符串，
		User user = new User("CSDN", "123456");
		redisService.setString("userInfo", JSON.toJSONString(user));
		log.info("用户信息：{}", redisService.getString("userInfo"));


		//测试redis的hash类型
		redisService.setHash("user","name", JSON.toJSONString(user));
		log.info("用户姓名：{}", redisService.getHash("user","name"));

		//测试redis的list类型
		redisService.setList("list", "football");
		redisService.setList("list", "basketball");
		List<String> valList = redisService.getList("list",0,-1);
		for(String value :valList){
			log.info("list中有：{}", value);
		}
		//测试设置key失效时间
		//        redisService.setTimeOut("CSDN", 9);
		//让当前线程休眠10秒
		//        try {
		//            Thread.sleep(10000);
		//        } catch (InterruptedException e) {
		//            e.printStackTrace();
		//        }
		log.info("CSDN：", redisService.getString("CSDN"));
	}

}
