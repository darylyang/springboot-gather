package com.daryl.redis.model;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@ToString
@Data
public class User implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String username;
    private String password;
    private String email;
    private String nickname;
    private String regTime;

    public User() {
        super();
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public User(String email, String nickname, String password, String username, String regTime) {
        super();
        this.email = email;
        this.nickname = nickname;
        this.password = password;
        this.username = username;
        this.regTime = regTime;
    }

}