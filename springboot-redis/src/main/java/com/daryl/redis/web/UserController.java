package com.daryl.redis.web;

import com.alibaba.fastjson.JSON;
import com.daryl.redis.model.User;
import com.daryl.redis.service.RedisService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.UUID;

@Slf4j
@RestController
public class UserController {

    @Resource
    private RedisService redisService;

    @RequestMapping("/getUser")
    @Cacheable(value = "user-key")
    public User getUser() {
        User user = new User("aa@126.com", "aa", "aa123456", "aa", "123");
        System.out.println("若下面没出现“无缓存的时候调用”字样且能打印出数据表示测试成功");
        return user;
    }

    @RequestMapping("/uid")
    public String uid(HttpSession session) {
        UUID uid = (UUID) session.getAttribute("uid");
        if (uid == null) {
            uid = UUID.randomUUID();
        }
        session.setAttribute("uid", uid);
        return session.getId();
    }

    @GetMapping("test")
    public void test() {
        //测试redis的string类型
        redisService.setString("weichat","程序员私房菜");
        log.info("我的微信公众号为：{}", redisService.getString("weichat"));

        // 如果是个实体，我们可以使用json工具转成json字符串，
        User user = new User("CSDN", "123456");
        redisService.setString("userInfo", JSON.toJSONString(user));
        log.info("用户信息：{}", redisService.getString("userInfo"));


        //测试redis的hash类型
        redisService.setHash("user","name", JSON.toJSONString(user));
        log.info("用户姓名：{}", redisService.getHash("user","name"));

        //测试redis的list类型
        redisService.setList("list", "football");
        redisService.setList("list", "basketball");
        List<String> valList = redisService.getList("list",0,-1);
        for(String value :valList){
            log.info("list中有：{}", value);
        }
        //测试设置key失效时间
        //        redisService.setTimeOut("CSDN", 9);
        //让当前线程休眠10秒
        //        try {
        //            Thread.sleep(10000);
        //        } catch (InterruptedException e) {
        //            e.printStackTrace();
        //        }
        log.info("CSDN：", redisService.getString("CSDN"));
    }

}