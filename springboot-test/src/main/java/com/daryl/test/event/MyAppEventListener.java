package com.daryl.test.event;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/3/23 0:38
 */
@Slf4j
@Component
public class MyAppEventListener implements ApplicationListener<MyAppEvent> {
    @Override
    public void onApplicationEvent(MyAppEvent event) {
        log.debug("MyAppEventListener监听到MyAppEvent事件发生");
        log.debug("事件source:"+event.getSource());
        log.debug("事件timestamp:"+event.getTimestamp());
        log.debug("事件class:"+event.getClass());
    }
}
