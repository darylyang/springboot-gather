package com.daryl.test.event;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/3/23 0:48
 */
@Component
public class SpringContextHelper implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        SpringContextHelper.applicationContext = applicationContext;
    }

    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public static Object getBean(String name) {
        return applicationContext.getBean(name);
    }

    public static <T> T getBean(String name, Class  requiredClass){
        return (T) applicationContext.getBean(name, requiredClass);
    }
    public  static <T> T getBean(Class requiredType){
        return (T) applicationContext.getBean(requiredType);
    }

}
