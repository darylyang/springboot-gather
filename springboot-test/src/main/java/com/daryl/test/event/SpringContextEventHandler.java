package com.daryl.test.event;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.*;
import org.springframework.stereotype.Component;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/3/23 6:57
 */
@Slf4j
@Component
public class SpringContextEventHandler {

    @EventListener
    public void stopped(ContextStoppedEvent event){
        log.debug("SpringContextEventHandler stopped执行");
    }

    @EventListener
    public void started(ContextStartedEvent event){
        log.debug("SpringContextEventHandler started执行");
    }

    @EventListener
    public void refreshed(ContextRefreshedEvent event){
        log.debug("SpringContextEventHandler refreshed执行");

        SpringContextHelper.getApplicationContext().publishEvent(new MyAppEvent("这是一段事件消息qqqqqqqqqqqqqqq"));

    }
    @EventListener
    public void closed(ContextClosedEvent event){
        log.debug("SpringContextEventHandler closed执行");
    }


}
