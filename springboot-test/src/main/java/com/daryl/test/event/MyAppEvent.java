package com.daryl.test.event;

import org.springframework.context.ApplicationEvent;

/**
 * 自定义事件类
 *
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/3/23 0:38
 */
public class MyAppEvent extends ApplicationEvent {

    public MyAppEvent(Object source) {
        super(source);
    }
}
