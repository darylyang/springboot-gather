package com.daryl.test.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.stereotype.Component;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/3/22 23:46
 */
@Slf4j
@Component
public class SpringClosedListener implements ApplicationListener<ContextClosedEvent> {
    
    @Override
    public void onApplicationEvent(ContextClosedEvent contextClosedEvent) {
        log.debug("============SpringClosedListener 执行===========");
    }

}
