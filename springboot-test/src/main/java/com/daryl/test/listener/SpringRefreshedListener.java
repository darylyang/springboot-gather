package com.daryl.test.listener;

import com.daryl.test.event.MyAppEvent;
import com.daryl.test.event.SpringContextHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/3/22 23:46
 */
@Slf4j
@Component
public class SpringRefreshedListener implements ApplicationListener<ContextRefreshedEvent> {
    
    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        log.debug("============SpringRefreshedListener 执行===========");
        SpringContextHelper.getApplicationContext().publishEvent(new MyAppEvent("这是一段事件消息wwwwwwww"));
    }

}
