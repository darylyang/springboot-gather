package com.daryl.test;

import com.daryl.test.event.MyAppEventListener;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootTestApplication {

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(SpringbootTestApplication.class);
        app.addListeners(new MyAppEventListener());
        app.run(args);
//        SpringApplication.run(SpringbootTestApplication.class, args);
    }

}
