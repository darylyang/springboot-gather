package com.daryl.test.collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/4/25 20:41
 */
public class ListTest {

    public static void main(String[] args) {
        List<String> list = new ArrayList<>(2);
        list.add("guan");
        list.add("bao");
        list.add("刘");
        String[] strings = list.toArray(new String[0]);
        System.out.println(Arrays.toString(strings));
    }
}
