package com.daryl.test;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.util.Arrays;
import java.util.List;

/**
 * 在Java中，文件或是目录习惯用java.io.File对象来表示，但是File类有很多缺陷，比如它的很多方法不能抛出异常，它的delete方法经常莫名其妙的失败等，旧的File类经常是程序失败的根源。
 * 因此在Java7中有了更好的替代：java.nio.file.Path及java.nio.file.Files。
 * <p>
 * Path接口的名字非常恰当，就是表示路径的，API中讲Path对象可以是一个文件，一个目录，或是一个符号链接，也可以是一个根目录。用法很简单。创建Path并不会创建物理文件或是目录，path实例经常引用并不存在的物理对象，要真正创建文件或是目录，需要用到Files类。
 * <p>
 * Files类是一个非常强大的类，它提供了处理文件和目录以及读取文件和写入文件的静态方法。可以用它创建和删除路径。复制文件。检查路径是否存在等。此外。Files还拥有创建流对象的方法。
 *
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/4/7 22:52
 */
public class FileUtilsTest {

    @Test
    public void testPath() {
        String fileName = "E:\\aa\\txt\\1.txt";

        /*------------------------1.创建一个路径-------------------------*/
        // 并没有实际创建路径，而是一个指向路径的引用
//        Path path = FileSystems.getDefault().getPath("E:", "aa\\txt\\1.txt");
        Path path = FileSystems.getDefault().getPath(fileName);
        // Paths类提供了这个快捷方法，直接通过它的静态get方法创建path
        Path path1 = Paths.get(fileName);

        if (path == path1) {
            System.out.println("path == path1");
        }
        if (path.equals(path1)) {
            System.out.println("path.equals(path1) -> " + path.equals(path1));
        }
        System.out.println("path.compareTo(path1) -> " + path.compareTo(path1));

        // 获取全路径 path.toString() -> E:\aa\txt\1.txt
        System.out.println("path.toString() -> " + path.toString());
        // 获取文件名 path.getFileName() -> 1.txt
        System.out.println("path.getFileName() -> " + path.getFileName());
        // 获取父目录 path.getParent() -> E:\aa\txt
        System.out.println("path.getParent() -> " + path.getParent());
        // 获取根目录 path.getRoot() -> E:\
        System.out.println("path.getRoot() -> " + path.getRoot());
        // 获取目录中元素的个数 path.getNameCount() -> 3
        System.out.println("path.getNameCount() -> " + path.getNameCount());
        // 获取路径中第一个元素名 path.getName(0) -> aa
        System.out.println("path.getName(0) -> " + path.getName(0));
        // 获取路径中第二个元素名 path.getName(1) -> txt
        System.out.println("path.getName(1) -> " + path.getName(1));

    }

    @Test
    public void testPath2File() {
        File file = new File("E:\\aa\\txt\\1.txt");

        Path filePath = file.toPath();
        File file1 = filePath.toFile();

        URI uri = file.toURI();
        Path path = Paths.get(uri);
    }


    /**
     * 创建和删除文件与目录
     *
     * @throws IOException
     */
    @Test
    public void testFiles1() throws IOException {
        String dirName = "E:\\aa\\txt\\11";
        String fileName = "E:\\aa\\txt\\11\\00.txt";

        Path pathDir = Paths.get(dirName);
        Path pathFile = Paths.get(fileName);

        // 创建目录
        Path dirPath = Files.createDirectory(pathDir);
        // 创建文件
        Path filePath = Files.createFile(pathFile);

        // 直接删除文件
        Files.delete(pathFile);

        // 先判断是否存在，存在再删
        boolean deleteIfExists = Files.deleteIfExists(pathFile);
        System.out.println("deleteIfExists -> " + deleteIfExists);
    }

    /**
     * 复制和移动文件
     *
     * @throws IOException
     */
    @Test
    public void testFiles2() throws IOException {

        // 复制文件，StandardCopyOption.REPLACE_EXISTING表示：如果目标文件存在，则替换它
        // Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
        //        copy(Path, Path, CopyOption...)
        //        copy(InputStream, OutputStream)
        //        copy(InputStream, Path, CopyOption...)
        //        copy(Path, OutputStream)
        Path copy = Files.copy(Paths.get("E:\\aa\\txt\\a.txt"), Paths.get("E:\\aa\\txt\\a-1.txt"), StandardCopyOption.REPLACE_EXISTING);
        System.out.println("copy.toString() -> " + copy.toString());

        // 移动文件
        //        Files.move(source2, target2, StandardCopyOption.REPLACE_EXISTING);
        Path move = Files.move(Paths.get("E:\\aa\\txt\\a.txt"), Paths.get("E:\\aa\\txt\\a-2.txt"), StandardCopyOption.REPLACE_EXISTING);
        System.out.println("move.toString() -> " + move.toString());
    }

    /**
     * 文件的读取和写入
     * Files类读取和写入的都是小型二进制文件和文本文件，大型文件要用流。
     *
     * @throws IOException
     */
    @Test
    public void testFiles3() throws IOException {
//        // 读取文本文件，按charset编码,textFile为要读取文件的路径，返回List<String>型的数据
//        Files.readAllLines(textFile, charset);
//        // 读取二进制文件 ，返回二进制数据的byte[]数组
//        Files.readAllBytes(path);
//
//        // 写入二进制数据。往path路径下的文件写入byte[]型的bytes数据，返回的是一个path路径
//        Files.write(path, bytes);
//        // 写入文本数据 。把List<String> 型的lines数据写入到文件textFile中，以charset编码形式。
//        Files.write(textFile, lines, charset);

        Charset charset = Charset.forName("UTF-8");
        String fileName = "E:\\aa\\txt\\1.txt";
        Path textFile = Paths.get(fileName);

        String line1 = "你好：";
        String line2 = "Files";
        List<String> lines = Arrays.asList(line1, line2);
        Files.write(textFile, lines, charset);

        List<String> linesRead = Files.readAllLines(textFile, charset);

        for (String s : linesRead) {
            System.out.println(s);
        }



    }

}
