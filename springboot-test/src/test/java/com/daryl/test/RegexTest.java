package com.daryl.test;

import org.junit.jupiter.api.Test;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/4/8 21:09
 */
public class RegexTest {

    /**
     * 提取括号内的字符串
     */
    @Test
    public void testRegex1() {
        String str = "(test)";
        String restr = str.replaceAll("\\(.*?\\)|\\{.*?}|\\[.*?]|（.*?）", "");
        System.out.println(restr);
    }
}
