package com.daryl.websocket.weblog;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/9/20 19:48
 */
@Controller
@RequestMapping
public class WeblogController {

    private Logger logger = LoggerFactory.getLogger(WeblogController.class);

    int info = 1;

    @Scheduled(fixedRate = 1000)
    public void outputLogger() {
        logger.info("测试日志输出" + info++);
        throw new RuntimeException();
    }

    @RequestMapping("/weblog")
    public String weblog() {
        logger.debug("访问了weblog");
        return "weblog";
    }
}
