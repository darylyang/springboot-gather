package com.daryl.websocket.demo;

import org.springframework.context.annotation.Bean;
import org.yeauty.standard.ServerEndpointExporter;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/9/17 22:13
 */
//@Configuration
public class WebSocketConfig {
    @Bean
    public ServerEndpointExporter serverEndpointExporter() {
        return new ServerEndpointExporter();
    }
}
