package com.daryl.websocket.demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/9/17 22:59
 */
@Controller
@RequestMapping
public class WebsocketController {

    @GetMapping("demo")
    public String websocket() {
        return "index";
    }
}
