package com.daryl.easyexcel.core.multiplesheets;

import lombok.Data;

/**
 * @author Jiaju Zhuang
 */
@Data
public class MultipleSheetsData {
    private String title;
}
