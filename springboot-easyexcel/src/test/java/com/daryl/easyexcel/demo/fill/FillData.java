package com.daryl.easyexcel.demo.fill;

import lombok.Data;

/**
 * @author Jiaju Zhuang
 */
@Data
public class FillData {
    private String name;
    private double number;
}
