package com.daryl.ehcache;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.concurrent.TimeUnit;

@SpringBootTest
class SpringbootEhcacheApplicationTests {

    @Test
    void contextLoads() {
    }

    @Autowired
    private CacheService<String,String> cacheService;


    @Test
    void test1() {
        cacheService.put("hello", "yao");
        System.out.println(cacheService.get("hello"));

        try {
            TimeUnit.SECONDS.sleep(4);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(cacheService.get("hello"));

        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(cacheService.get("hello"));

        cacheService.put("hello", "henry");
        System.out.println(cacheService.get("hello"));
    }
}
