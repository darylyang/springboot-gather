package com.daryl.ehcache.spring;

import com.daryl.ehcache.CacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @author yys1892
 */
@Service
@ConditionalOnProperty(name = "cache.mode", havingValue = "springCache", matchIfMissing = true)
public class SpringCacheService implements CacheService<String, String> {

    private CacheManager cacheManager;

    @Autowired
    public SpringCacheService(CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }

    @CachePut(key = "#p0", cacheNames = "springCache")
    @Override
    public String put(String s, String s2) {
        Objects.requireNonNull(cacheManager.getCache("springCache"))
                .put(s, s2);
        return s2;
    }

    @Cacheable(cacheNames = "springCache", key = "#p0")
    @Override
    public String get(String s) {
        return null;
    }

}
