package com.daryl.ehcache.JSR;

import com.daryl.ehcache.CacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import javax.cache.Cache;

/**
 * @author yys1892
 */
@Service
@ConditionalOnProperty(name = "cache.mode", havingValue = "jCache")
public class JCacheService implements CacheService<String, String> {

    private Cache<String, String> jCache;

    @Autowired
    public JCacheService(Cache<String, String> jCache) {
        this.jCache = jCache;
    }

    @Override
    public String put(String key, String value) {
        jCache.put(key, value);
        return value;
    }

    @Override
    public String get(String s) {
        return jCache.get(s);
    }
}
