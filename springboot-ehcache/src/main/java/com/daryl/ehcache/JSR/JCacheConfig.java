package com.daryl.ehcache.JSR;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.cache.Cache;
import javax.cache.CacheManager;
import javax.cache.Caching;
import javax.cache.configuration.MutableConfiguration;
import javax.cache.expiry.CreatedExpiryPolicy;
import javax.cache.expiry.Duration;
import javax.cache.spi.CachingProvider;

/**
 * 使用JSR107操作ehcache
 * JSR107（Jcache）类似于SLF4J，是一个中立的标准，提供一个统一的接口来操作各个不同的缓存实现。要想使用JSR107操作缓存，必须将Jcache和具体
 * 缓存组件加载到项目
 *
 * @author yys1892
 */
@Configuration
@ConditionalOnProperty(name = "cache.mode", havingValue = "jCache")
public class JCacheConfig {
    @Bean
    public CacheManager jCacheManager() {
        // 在classpath下找CachingProvider的实现，如果有多个实现，可以指定具体类型获取 getCachingProvider(类名);
        // 其实这一步相当于获取加载到项目里的缓存类型，本例是ehcache，接下来的操作都会对应到ehcache上同等的操作
        CachingProvider cachingProvider = Caching.getCachingProvider();
        // 获取CacheManage，创建和淘汰缓存都需要通过CacheManage来进行操作
        return cachingProvider.getCacheManager();

    }

    @Bean
    public Cache<String, String> jCache() {
        // 创建缓存的配置类
        // new MutableConfiguration<String, String> 泛型必须要配 不然会报错
        MutableConfiguration<String, String> configuration = new MutableConfiguration<String, String>()
                // 设置缓存的key和value的类型
                .setTypes(String.class, String.class)
                // 设置过期策略
                .setExpiryPolicyFactory(CreatedExpiryPolicy.factoryOf(Duration.ONE_MINUTE))
                // 设置按值存储还是按引用存储
                .setStoreByValue(false);
        // 创建缓存
        return jCacheManager().createCache("jCache", configuration);
    }

}
