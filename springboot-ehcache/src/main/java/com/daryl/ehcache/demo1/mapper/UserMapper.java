package com.daryl.ehcache.demo1.mapper;

import com.daryl.ehcache.demo1.entity.User;

import java.util.List;

public interface UserMapper {

    List<User> getUsers();

    int addUser(User user);

    List<User> getUsersByName(String userName);
}
