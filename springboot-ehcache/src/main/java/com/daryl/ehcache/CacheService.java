package com.daryl.ehcache;

public interface CacheService<T, E> {
    /**
     * 增加一个缓存，如果已存在key相同的则更新value
     *
     * @param t
     * @param e
     * @return
     */
    E put(T t, E e);

    /**
     * 根据key获取缓存
     *
     * @param t
     * @return
     */
    E get(T t);

}
