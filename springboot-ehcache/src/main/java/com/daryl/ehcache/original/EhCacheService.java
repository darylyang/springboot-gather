package com.daryl.ehcache.original;

import com.daryl.ehcache.CacheService;
import org.ehcache.Cache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

@Service
@ConditionalOnProperty(name = "cache.mode", havingValue = "ehCache")
public class EhCacheService implements CacheService<String, String> {

    private Cache<String, String> ehCache;

    @Autowired
    public EhCacheService(Cache<String, String> ehCache) {
        this.ehCache = ehCache;
    }

    @Override
    public String put(String s, String s2) {
        ehCache.put(s, s2);
        return s2;
    }

    @Override
    public String get(String s) {
        return ehCache.get(s);
    }
}
