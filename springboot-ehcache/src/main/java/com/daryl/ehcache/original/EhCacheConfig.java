package com.daryl.ehcache.original;

import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 使用ehCache原生的api操作缓存
 *
 * @author yys1892
 */
@Configuration
@ConditionalOnProperty(name = "cache.mode", havingValue = "ehCache")
public class EhCacheConfig {
    /**
     * 创建cacheManage
     *
     * @return
     */
    @Bean
    public CacheManager ehCacheManager() {
        return CacheManagerBuilder.newCacheManagerBuilder()
                .build(true);
    }

    /**
     * 创建cache
     *
     * @return
     */
    @Bean
    public Cache<String, String> ehCache() {
        return ehCacheManager().createCache("ehCache",
                CacheConfigurationBuilder.newCacheConfigurationBuilder(String.class, String.class, ResourcePoolsBuilder.heap(10)));
    }

}
