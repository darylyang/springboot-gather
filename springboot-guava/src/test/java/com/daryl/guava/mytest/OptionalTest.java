package com.daryl.guava.mytest;

import com.google.common.base.Optional;
import junit.framework.TestCase;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/4/21 0:29
 */
public class OptionalTest extends TestCase {

    public void testOf() {
        assertEquals("training", Optional.of("training").get());
    }

    public void testOf_null() {
        try {
            Optional.of(null);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    public void testAbsent() {
        Optional<String> optionalName = Optional.absent();
        assertFalse(optionalName.isPresent());
    }

    public void testFromNullable() {
        Optional<String> optionalName = Optional.fromNullable("bob");
        assertEquals("bob", optionalName.get());
    }

    public void testFromNullable_null() {
        // not promised by spec, but easier to test
        assertSame(Optional.absent(), Optional.fromNullable(null));
    }

    public void testIsPresent_no() {
        assertFalse(Optional.absent().isPresent());
    }

    public void testIsPresent_yes() {
        assertTrue(Optional.of("training").isPresent());
    }
}
