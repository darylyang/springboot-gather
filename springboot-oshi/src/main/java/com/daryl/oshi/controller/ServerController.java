package com.daryl.oshi.controller;

import com.daryl.oshi.config.ServerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 服务器监控
 *
 * @author daryl
 * @date 2019/1/30 17:40
 */
@Controller
@RequestMapping("/")
public class ServerController {

    private static final Logger logger = LoggerFactory.getLogger(ServerController.class);

    private String prefix = "monitor/server/";

    @RequestMapping("/")
    public String server(ModelMap map) throws Exception {
        ServerConfig serverConfig = new ServerConfig();
        serverConfig.copyTo();
        map.put("server", serverConfig);
        logger.info("server------");
        return prefix + "server";
    }

}
