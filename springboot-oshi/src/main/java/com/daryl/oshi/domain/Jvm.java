package com.daryl.oshi.domain;

import com.daryl.oshi.util.ArithUtils;
import com.daryl.oshi.util.DateUtils;
import lombok.Setter;

import java.lang.management.ManagementFactory;

/**
 * JVM相关信息
 *
 * @author daryl
 * @date 2019/1/30 16:00
 */
@Setter
public class Jvm {
    /**
     * 当前JVM占用的内存总数(M)
     */
    private double total;

    /**
     * JVM最大可用内存总数(M)
     */
    private double max;

    /**
     * JVM空闲内存(M)
     */
    private double free;

    /**
     * JDK版本
     */
    private String version;

    /**
     * JDK路径
     */
    private String home;

    public double getTotal() {
        return ArithUtils.div(total, (1024 * 1024), 2);
    }

    public double getMax() {
        return ArithUtils.div(max, (1024 * 1024), 2);
    }

    public double getFree() {
        return ArithUtils.div(free, (1024 * 1024), 2);
    }

    public String getVersion() {
        return version;
    }

    public String getHome() {
        return home;
    }

    public double getUsed() {
        return ArithUtils.div(total - free, (1024 * 1024), 2);
    }

    public double getUsage() {
        return ArithUtils.mul(ArithUtils.div(total - free, total, 4), 100);
    }

    /**
     * 获取JDK名称
     */
    public String getName() {
        return ManagementFactory.getRuntimeMXBean().getVmName();
    }

    /**
     * JDK启动时间
     */
    public String getStartTime() {
        return DateUtils.parseDateToStr(DateUtils.YYYY_MM_DD_HH_MM_SS, DateUtils.getServerStartDate());
    }

    /**
     * JDK运行时间
     */
    public String getRunTime() {
        return DateUtils.getDatePoor(DateUtils.getNowDate(), DateUtils.getServerStartDate());
    }
}
