package com.daryl.oshi.domain;

import com.daryl.oshi.util.ArithUtils;
import lombok.Setter;

/**
 * CPU相关信息
 *
 * @author daryl
 * @date 2019/1/30 16:00
 */
@Setter
public class Cpu {
    /**
     * 核心数
     */
    private int cpuNum;

    /**
     * CPU总的使用率
     */
    private double total;

    /**
     * CPU系统使用率
     */
    private double sys;

    /**
     * CPU用户使用率
     */
    private double used;

    /**
     * CPU当前等待率
     */
    private double wait;

    /**
     * CPU当前空闲率
     */
    private double free;

    public int getCpuNum() {
        return cpuNum;
    }


    public double getTotal() {
        return ArithUtils.round(ArithUtils.mul(total, 100), 2);
    }


    public double getSys() {
        return ArithUtils.round(ArithUtils.mul(sys / total, 100), 2);
    }


    public double getUsed() {
        return ArithUtils.round(ArithUtils.mul(used / total, 100), 2);
    }


    public double getWait() {
        return ArithUtils.round(ArithUtils.mul(wait / total, 100), 2);
    }


    public double getFree() {
        return ArithUtils.round(ArithUtils.mul(free / total, 100), 2);
    }

}
