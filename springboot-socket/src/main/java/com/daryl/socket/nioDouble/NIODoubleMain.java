package com.daryl.socket.nioDouble;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/**
 * 通过 NIODoubleClient sendMsg 输入表达式
 * NIODoubleServerHandler 中的 CalculatorUtil.cal(String) 算出结果并响应结果给 client
 *
 *
 *
 * 服务器已启动 >>>>>>>> 端口号：8888
 * 1+2
 * 服务器收到消息：1+2
 * 客户端收到消息：3
 * 2+3
 * 服务器收到消息：2+3
 * 客户端收到消息：5
 */
public class NIODoubleMain {

    public static void main(String[] args) throws Exception {
        // 启动服务器  
        NIODoubleServer.start();

        // Thread.sleep(100)
        TimeUnit.MILLISECONDS.sleep(100);

        // 启动客户端
        NIODoubleClient.start();

        // 输入表达式 即可 通过CalculatorUtil.cal(String) 算出结果并响应
        while (NIODoubleClient.sendMsg(new Scanner(System.in).nextLine())) {
        }
    }

}
