package com.daryl.socket.util;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class CalculatorUtil {

    private final static ScriptEngine ENGINE = new ScriptEngineManager().getEngineByName("JavaScript");

    public static Object cal(String expression) {
        try {
            return ENGINE.eval(expression);
        } catch (ScriptException e) {
            e.printStackTrace();
        }
        return null;
    }

}
