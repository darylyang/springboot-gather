package com.daryl.transaction.entity;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/4/19 1:09
 */
@Data
@Builder
@ToString
public class User {

    private Long id;
    private String username;
    private String password;

}
