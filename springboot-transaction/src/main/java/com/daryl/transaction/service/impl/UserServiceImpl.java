package com.daryl.transaction.service.impl;

import com.daryl.transaction.dao.UserMapper;
import com.daryl.transaction.entity.User;
import com.daryl.transaction.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.sql.SQLException;
import java.util.List;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/4/19 10:25
 */
@Slf4j
@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserMapper userMapper;

    @Override
    @Transactional
    public void insertUser(User user) {
        System.out.println(getAll());
        // 插入用户信息
        userMapper.insertUser(user);
        // 手动抛出异常
        throw new RuntimeException();
    }

    @Override
    @Transactional
    public void insertUser1(User user) throws Exception{
        System.out.println(getAll());
        // 插入用户信息
        userMapper.insertUser(user);
        // 手动抛出异常
        throw new SQLException("数据库异常");
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void insertUser2(User user) throws Exception {
        System.out.println(getAll());
        // 插入用户信息
        userMapper.insertUser(user);
        // 手动抛出异常
        throw new SQLException("数据库异常");
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void insertUser3(User user) {
        System.out.println(getAll());
        try {
            // 插入用户信息
            userMapper.insertUser(user);
            // 手动抛出异常
            throw new SQLException("数据库异常");
        } catch (Exception e) {
            // 异常处理逻辑
            log.error("插入异常", getAll());
        }
        System.out.println(getAll());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public synchronized void insertUser4(User user) {
        System.out.println(getAll());
        // 插入用户信息
        userMapper.insertUser(user);
        System.out.println(getAll());
    }

    @Override
    public List<User> getAll() {
        return userMapper.getAll();
    }
}
