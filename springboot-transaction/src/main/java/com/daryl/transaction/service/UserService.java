package com.daryl.transaction.service;

import com.daryl.transaction.entity.User;

import java.util.List;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/4/19 10:25
 */
public interface UserService {

    void insertUser(User user);

    void insertUser1(User user) throws Exception;

    void insertUser2(User user) throws Exception;

    void insertUser3(User user);

    void insertUser4(User user);

    List<User> getAll();

}
