package com.daryl.transaction.controller;

import com.daryl.transaction.entity.User;
import com.daryl.transaction.service.UserService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Author: Daryl
 * @Email: darylliu@126.com
 * @Date: 2020/4/19 10:28
 */
@RestController
public class TestController {

    @Resource
    private UserService userService;

    /**
     * 正常测试
     *
     * @param user
     * @return
     */
    @PostMapping("/adduser")
    public String addUser(@RequestBody User user) throws Exception {
        user = User.builder().username("aaaa").password("aaaa").build();
        userService.insertUser(user);
        return "success";
    }

    /**
     *
     * @param user
     * @return
     * @throws Exception
     */
    @PostMapping("/adduser1")
    public String addUser1(@RequestBody User user) throws Exception {
        user = User.builder().username("bbbb").password("bbbb").build();
        userService.insertUser1(user);
        return "success";
    }

    /**
     * 测试异常并没有被捕获到
     *
     * @param user
     * @return
     * @throws Exception
     */
    @PostMapping("/adduser2")
    public String addUser2(@RequestBody User user) throws Exception {
        user = User.builder().username("cccc").password("cccc").build();
        userService.insertUser2(user);
        return "success";
    }

    /**
     * 测试异常被吃掉
     *
     * @param user
     * @return
     * @throws Exception
     */
    @PostMapping("/adduser3")
    public String addUser3(@RequestBody User user) {
        user = User.builder().username("dddd").password("dddd").build();
        userService.insertUser3(user);
        return "success";
    }

    @PostMapping("/adduser4")
    public String addUser4() {
        User user = User.builder().username("eeee").password("eeee").build();
        userService.insertUser4(user);
        return "success";
    }
}
